from unittest import mock

from skechers_loyalty.containers import loyalty_container
from skechers_loyalty.domain.service.loyalty_service import LoyaltyService


class TestSessionM:

    @mock.patch('ecom.domain.service.parameter_service.ParameterService.get_parameter')
    def test_get_user(self, get_parameter):
        get_parameter.return_value = "123"
        service: LoyaltyService = loyalty_container.provide(LoyaltyService)
        response = service.get_user('81cbb616-98c8-11ec-89c9-f0939a968349', '600')
        assert response
