import logging
from skechers_loyalty.containers import loyalty_container
from skechers_loyalty.domain.service.loyalty_service import LoyaltyService

LOGGER = logging.getLogger(__name__)


class TestContainer:

    def test_container_startup(self):
        loyalty_service = loyalty_container.provide(LoyaltyService)
        print(loyalty_service)
        assert loyalty_service is not None
