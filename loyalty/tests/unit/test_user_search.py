import asyncio
import copy
import json
from typing import Tuple
from botocore import stub
import pytest
import requests
import requests_mock
from fixtures import context, lambda_module, get_order, get_product # pylint: disable=import-error
from helpers import compare_dict, mock_table # pylint: disable=import-error,no-name-in-module


lambda_module = pytest.fixture(scope="module", params=[{
    "function_dir": "create_order",
    "module_name": "main",
    "environ": {
        "ENVIRONMENT": "test",
        "DELIVERY_API_URL": "mock://DELIVERY_API_URL",
        "PAYMENT_API_URL": "mock://PAYMENT_API_URL",
        "PRODUCTS_API_URL": "mock://PRODUCTS_API_URL",
        "TABLE_NAME": "TABLE_NAME",
        "POWERTOOLS_TRACE_DISABLED": "true"
    }
}])(lambda_module)
context = pytest.fixture(context)



def test_validate_user_search():
    """
    Test validate_user_search()
    """

    assert True

