

import asyncio
import concurrent
from crypt import methods
import datetime
import json
import os
from typing import List, Tuple
import uuid
from chalice import Chalice
import jsonschema
import requests
from aws_lambda_powertools.tracing import Tracer # pylint: disable=import-error
from aws_lambda_powertools.logging.logger import Logger # pylint: disable=import-error
from aws_lambda_powertools import Metrics # pylint: disable=import-error
from aws_lambda_powertools.metrics import MetricUnit # pylint: disable=import-error

from skechers_loyalty.containers import loyalty_container

app = Chalice(app_name="LoyaltyAPIBase")

ENVIRONMENT = os.environ["ENVIRONMENT"]
SCHEMA_FILE = os.path.join(os.path.dirname(__file__), "schema.json")


logger = Logger() # pylint: disable=invalid-name
tracer = Tracer() # pylint: disable=invalid-name
metrics = Metrics(namespace="ecommerce.orders") # pylint: disable=invalid-name


@app.route('/advSearchUser', methods = ['GET'])
def search_user():
    current_request = app.current_request
    query_params = current_request.query_params
    loyalty_container.loyalty_service().search_users()
    return {
        "phone": query_params.get("phone"),
        "test": True
    }

@app.route('/details', methods = ['GET'])
def get_user():
    current_request = app.current_request
    query_params = current_request.query_params
    webstore_num = current_request.headers.get('StoreId')
    response = loyalty_container.loyalty_service().get_user(query_params.get("loyalty_id"), webstore_num)
    return response
