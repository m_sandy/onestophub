import os
import logging

from ecom.apigateway import response
from skechers_loyalty.containers import loyalty_container
from skechers_loyalty.domain.service.loyalty_service import LoyaltyService

loyalty_service: LoyaltyService = loyalty_container.provide(LoyaltyService)
ENVIRONMENT = os.environ.get("ENVIRONMENT")
logger = logging.getLogger(__name__)


def handler(event, _):
    logger.debug({"message": "Event received", "event": event})
    # Retrieve the loyalty_id
    try:
        loyalty_id = event["queryStringParameters"]["loyalty_id"]
    except (KeyError, TypeError):
        logger.warning({"message": "Loyalty ID not found in the request"})
        return response("Missing loyalty_id", 400)

    web_store_num = event["headers"].get("StoreId")
    service_response = loyalty_service.get_user(loyalty_id, web_store_num)
    return response(service_response)
