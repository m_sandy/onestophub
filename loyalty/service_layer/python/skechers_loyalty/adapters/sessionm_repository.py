from cachetools.func import ttl_cache
from requests import Session
import logging
from requests.auth import HTTPBasicAuth

from ecom.domain.service.parameter_service import ParameterService
from ecom.domain.constants import ENV
from skechers_loyalty.domain.repository.loyalty_remote_repository import LoyaltyRemoteRepository

LOGGER = logging.getLogger(__name__)


class SessionMRepository(LoyaltyRemoteRepository):
    logging.basicConfig(encoding='utf-8', level=logging.INFO)

    CUSTOMER_API_URL_KEY = 'sessionm.api.v2.customers.urls'
    CUSTOMER_API_KEY_KEY = 'sessionm.api.v2.customers.keys'
    CUSTOMER_API_SECRET_KEY = 'sessionm.api.v2.customers.secrets'

    def __init__(self, parameter_service: ParameterService, config) -> None:
        self.__parameter_service = parameter_service
        self.__config = config
        self.__sessions = dict()

    def get_session(self, webstore_num: str) -> Session:
        request_session = self.__sessions.get(webstore_num)
        if not request_session:
            request_session = Session()
            api_key = self.get_customers_api_key(webstore_num)
            api_secret = self.get_customers_api_secret(webstore_num)
            basic_auth = HTTPBasicAuth(api_key, api_secret)
            request_session.auth = basic_auth
            self.__sessions[webstore_num] = request_session
        return request_session

    #     private val _customersApiUrlsMap: Map[String, String] = cMap("sessionm.api.v2.customers.urls")
    #   def customersApiUrl(webstorenum: String): String = _customersApiUrlsMap(webstorenum)

    def get_customers_api_url(self, webstore_num: str) -> str:
        # return self.__config.get(self.CUSTOMER_API_URL_KEY).get(webstore_num)
        sessiom = self.__config.get('sessionm')
        api = sessiom.get('api')
        v2 = api.get('v2')
        v2 = v2.get('customers')
        urls = v2.get('urls')
        store_url = urls.get(webstore_num)
        return self.__config.get('sessionm').get('api').get('v2').get('customers').get('urls').get(webstore_num)

    @ttl_cache(ttl=600)
    def get_customers_api_key(self, webstore_num: str) -> str:
        # return self.__config.get(self.CUSTOMER_API_KEY_KEY).get(webstore_num)
        # return self.__parameter_service.get_parameter(f"/environments/prod/ecommerce/sessionm/api_secret_en_us")
        # return self.__config.get('sessionm').get('api').get('v2').get('customers').get('urls').get(webstore_num)
        return self.__parameter_service.get_parameter(f"/environments/{ENV}/ecommerce/sessionm/api_key_en_us")

    def get_customers_api_secret(self, webstore_num: str) -> str:
        return self.__parameter_service.get_parameter(f"/environments/{ENV}/ecommerce/sessionm/api_secret_en_us")
        # return self.__config.get(self.CUSTOMER_API_SECRET_KEY).get(webstore_num)

    def get_user_by_uuid(self, loyalty_id: str, webstore_num: str):
        response = None
        api_url = self.get_customers_api_url(webstore_num)
        api_key = self.get_customers_api_key(webstore_num)
        request_url = f"{api_url}/priv/v1/apps/{api_key}/users/{loyalty_id}?user[user_profile]=true&show_identifiers=true"
        LOGGER.info(request_url)
        api_response = self.get_session(webstore_num).get(
            f"{api_url}/priv/v1/apps/{api_key}/users/{loyalty_id}?user[user_profile]=true&show_identifiers=true")
        if api_response.status_code == 200:
            response = api_response.json()
            # translate to DTO...
            # Multiple calls name, email, phone
            # send data
        else:
            response = api_response.text
        return response
