"""User containers module."""

import os
import sys
import pkg_resources
import pinject
import yaml
from pinject.object_graph import ObjectGraph

from ecom.containers import EcomBindingSpec
from skechers_loyalty.adapters.sessionm_repository import SessionMRepository
from skechers_loyalty.adapters.db_repository import DBRepository


class LoyaltyBindingSpec(pinject.BindingSpec):

    def configure(self, bind):
        bind('remote_repository', to_class=SessionMRepository)
        bind('local_repository', to_class=DBRepository)

    def provide_config(self):
        configuration = dict()
        env_path = ''
        if os.environ.get('ENV'):
            env_path = f"_{os.environ.get('ENV')}"
        app_resource_path = pkg_resources.resource_filename(
            'skechers_loyalty.resources', f"application{env_path}.yml")
        with open(app_resource_path, 'r') as file:
            configuration = yaml.safe_load(file)
        return configuration


def create_container() -> ObjectGraph:
    obj_graph = pinject.new_object_graph(modules=[sys.modules[__name__]],
                                         binding_specs=[EcomBindingSpec(), LoyaltyBindingSpec()])
    return obj_graph


loyalty_container: ObjectGraph = create_container()
