from typing import Optional

from skechers_loyalty.domain.dto.advanced_user_search_request import AdvancedUserSearchRequest

class LoyaltyRemoteRepository:

    def get_user_by_uuid(self, loyalty_id: str, webstore_num: str): ...