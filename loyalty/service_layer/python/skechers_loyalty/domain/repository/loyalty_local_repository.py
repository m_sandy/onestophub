from typing import List


class LoyaltyLocalRepository:

    def get_user(self, remote_loyalty_ids: List[str]): ...
