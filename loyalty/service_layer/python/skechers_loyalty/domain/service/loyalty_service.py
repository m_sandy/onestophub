from skechers_loyalty.domain.repository.loyalty_remote_repository import LoyaltyRemoteRepository
from skechers_loyalty.domain.repository.loyalty_local_repository import LoyaltyLocalRepository


class LoyaltyService:

    def __init__(self, remote_repository: LoyaltyRemoteRepository, local_repository: LoyaltyLocalRepository) -> None:
        self.__remote_repository = remote_repository
        self.__local_repository = local_repository

    def get_user(self, loyalty_id: str, webstore_num: str):
        """Get user details from Lotalty Service by loyalty ID.

        Args:
            loyalty_id (str): Unique id of the user in the loyalty service
            webstore_num (str): store number

        Returns:
            _type_: _description_
        """
        response = None
        if webstore_num:
            response = self.__remote_repository.get_user_by_uuid(loyalty_id, webstore_num)
            # dedupe and data compare
            # response processing
        return response
