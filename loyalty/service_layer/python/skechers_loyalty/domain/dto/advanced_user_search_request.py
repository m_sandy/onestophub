from typing import Optional
from pydantic import BaseModel
from typing import Optional

class AdvancedUserSearchRequest(BaseModel):

    phone: Optional[str] = None
    email: Optional[str] = None
    first_name: Optional[str] = None
    last_name: Optional[str] = None