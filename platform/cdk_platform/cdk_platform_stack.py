from aws_cdk import (
    Duration,
    aws_sqs as sqs, aws_ssm as ssm,
    CfnParameter,
    aws_apigateway as apigateway,
    aws_lambda as lambda_,
    aws_events as events,
    aws_cloudwatch as cloudwatch,
    aws_lambda_event_sources as lambda_event_source,
    aws_iam as iam,
    aws_events as events,
    aws_events_targets as targets, RemovalPolicy
)
from cdk_watchful import Watchful
from constructs import Construct
from skx_cdk.skx_base_stack import SkxBaseStack
from skx_cdk.construct.lambda_construct import SkxLambda
from skx_cdk.construct.api_gateway_construct import SkxApiGateway


class CdkPlatformStack(SkxBaseStack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        stack_name = self.stack_name

        # -------------- Lambda Setup -------------------------
        core_lib_layer = lambda_.LayerVersion(self, "CommonLibLayer",
                                              description="Common python packages for all services",
                                              removal_policy=RemovalPolicy.RETAIN,
                                              code=lambda_.Code.from_asset("lib_layer/"),
                                              layer_version_name=f"{stack_name.lower()}-core-libs"
                                              )

        shared_ecom_layer = lambda_.LayerVersion(self, "SharedEcomLayer",
                                                 removal_policy=RemovalPolicy.RETAIN,
                                                 code=lambda_.Code.from_asset("shared_layer/"),
                                                 layer_version_name=f"{stack_name.lower()}-shared-ecom"
                                                 )

        ssm.StringParameter(self, "CommonLibLayerArn",
                            parameter_name=f"/ecommerce/{self.skx_environment}/platform/common-lib-layer/arn",
                            description="Common lib layer Arn",
                            string_value=core_lib_layer.layer_version_arn)

        ssm.StringParameter(self, "SharedEcomLayerArn",
                            parameter_name=f"/ecommerce/{self.skx_environment}/platform/shared-ecom-layer/arn",
                            description="SharedEcomLayer Arn",
                            string_value=shared_ecom_layer.layer_version_arn)

        # -------------- Event Bridge Setup -------------------------
        event_bus = events.EventBus(self, "PlatformEventBus", event_bus_name=f"ecommerce-{self.skx_environment}")
        ssm.StringParameter(self, "EventBusName",
                            parameter_name=f"/ecommerce/{self.skx_environment}/platform/event-bus/name",
                            description="Platform event bus name",
                            string_value=event_bus.event_bus_name)

        ssm.StringParameter(self, "EventBusArn",
                            parameter_name=f"/ecommerce/{self.skx_environment}/platform/event-bus/arn",
                            description="Platform event bus Arn",
                            string_value=event_bus.event_bus_arn)

        # -------- Auth lambada function setup
        auth_lambda_construct = SkxLambda(self, "AuthFunction", code="src/authorizer/", handler="main.lambda_handler")
        ssm.StringParameter(self, "AuthFunctionName",
                            parameter_name=f"/ecommerce/{self.skx_environment}/platform/authorizer/name",
                            description="Platform Auth function name",
                            string_value=auth_lambda_construct.handler.function_name)

        ssm.StringParameter(self, "AuthFunctionArn",
                            parameter_name=f"/ecommerce/{self.skx_environment}/platform/authorizer/arn",
                            description="Platform event bus Arn",
                            string_value=f"{auth_lambda_construct.handler.function_arn}:{self.skx_environment}")

    def env_mapping(self) -> dict:
        env_mapping = {
            "dev": {
                "AuthFunctionProvisionedConcurrency": 1,
            },
            "test": {
                "AuthFunctionProvisionedConcurrency": 1,
            },
            "prod": {
                "AuthFunctionProvisionedConcurrency": 1,
            },
        }
        return env_mapping
