import boto3
from moto import mock_s3
import pytest
import logging
import os
from pubsub import pub
from ecom.containers import core_container
from ecom.domain.service.parameter_service import ParameterService
from ecom.domain.event import EventDetails, EventSubscriber

LOGGER = logging.getLogger(__name__)


@pytest.fixture(scope='function')
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
    os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
    os.environ['AWS_SECURITY_TOKEN'] = 'testing'
    os.environ['AWS_SESSION_TOKEN'] = 'testing'
    os.environ['AWS_DEFAULT_REGION'] = 'us-west-1'


@pytest.fixture(scope='function')
def event_bridge(aws_credentials):
    with mock_s3():
        yield boto3.client('events', region_name='us-west-1')


def listener1(arg1, arg2=None):
    print('Function listener1 received:')
    print('  arg1 =', arg1)
    print('  arg2 =', arg2)


def snoop(topicObj=pub.AUTO_TOPIC, **mesgData):
    print('topic "%s": %s' % (topicObj.getName(), mesgData))


class TestEcomContainer:

    def test_ecom_container_startup(self):
        p_service = core_container.provide(ParameterService)
        assert p_service is not None

    def test_config_load(self):
        assert True
        # config = loyalty_container.config
        # LOGGER.info(type(loyalty_container.config))
        # assert config.get('sessionm.api.v2.customers.urls').get('600') == 'https://api-skechersus.stg-sessionm.com'
        os.environ['EVENT_BUS_NAME'] = 'MOCK_BUS'
        event_details = EventDetails(
            source="ecommerce.orders",
            type="OrderReceived",
            data={}
        )
        assert event_details.event_bus_name == 'MOCK_BUS'

    @mock_s3
    def test_events(self, event_bridge):
        os.environ['EVENT_BUS_NAME'] = 'MOCK_BUS'
        # We need to create the event bus since this is all in Moto's 'virtual' AWS account
        event_bridge.create_event_bus(Name='MOCK_BUS', EventSourceName="ecommerce.orders")

        # pub.sendMessage(EventSubscriber.PLATFORM_TOPIC_NAME, events=[EventDetails(
        #     source="ecommerce.orders",
        #     type="OrderReceived",
        #     data={}
        # )])
