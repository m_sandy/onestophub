import json
from ecom.util.aws.cloudwatch_metrics import CloudwatchDashboardBuilder, LambdaDashboardConfig, ApiDashboardConfig, \
    CustomKeyMetric

STACK_NAME = "test"


class TestCloudwatchDashboard:

    def test_dashboard_builder(self):
        create_resource_function = "ecommerce-orders-CreateOrderFunction-qS2e3XtknXSM"
        api_name = "ecommerce-orders-api"

        lambdas_config = [
            LambdaDashboardConfig(
                name=create_resource_function,
                label='CreateOrder',
                show_in_key_metrics=True,
                show_in_traffic_metrics=True,
                show_in_latency_and_duration_metrics=True,
                show_in_errors=True
            )
        ]
        apis_config = [
            ApiDashboardConfig(
                name=api_name,
                label='Orders API',
                show_in_key_metrics=True,
                show_in_traffic_metrics=True,
                show_in_latency_and_duration_metrics=True,
                show_in_errors=True
            )]
        custom_metrics = [CustomKeyMetric(name="orderCreated", label="Orders Created"),
                          CustomKeyMetric(name="orderFailed", label="Orders Failed")]

        """Entrypoint for the Lambda function"""
        builder = CloudwatchDashboardBuilder(STACK_NAME, lambdas_config, apis_config, custom_metrics)
        dashboard_body = builder.get_dashboard_body()
        print(json.dumps(dashboard_body))

        assert dashboard_body
