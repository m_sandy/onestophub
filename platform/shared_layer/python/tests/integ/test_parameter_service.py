import logging
from ecom.containers import create_container
from ecom.domain.service.parameter_service import ParameterService

container = create_container()
logger = logging.getLogger(__name__)

class TestParamterService:

    def test_get_valid_parameter(self):
        import os
        os.environ['AWS_PROFILE'] = 'sandy'
        from ecom.domain.service.parameter_service import ParameterService
        host_name = container.provide(ParameterService).get_parameter('/environments/prod/ecommerce/sessionm/api_hostname_en_us')
        logger.debug(host_name)
        assert host_name

    def test_get_missing_parameter(self):
        host_name = container.provide(ParameterService).get_parameter('Missing_param')
        assert host_name is None