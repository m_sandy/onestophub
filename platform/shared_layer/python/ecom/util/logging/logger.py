from aws_lambda_powertools import Logger as AWSLogger


class Logger(AWSLogger):
    """Util class logging, this is to decouple the AWS related code.

    """
    pass
