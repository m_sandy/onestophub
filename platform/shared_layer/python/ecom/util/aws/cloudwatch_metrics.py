"""
Builder class for Cloudwatch dashboard.
"""
import os
from typing import List, Optional, Union
from pydantic import BaseModel, validator
import random

from ecom.domain.constants import ENV, DEPLOYMENT_REGION
from ecom.util.logging import Logger
from ecom.adapter.aws.cloudwatch_adapter import CloudWatch

logger = Logger(child=True)

DASHBOARD_COLORS = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
                    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                    '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
                    '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                    '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
                    '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                    '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
                    '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                    '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
                    '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF']


class LambdaDashboardConfig(BaseModel):
    name: str
    label: str
    color: Optional[str]
    show_in_key_metrics: bool = False
    show_in_traffic_metrics: bool = False
    show_in_key_metric_errors: bool = True
    show_in_latency_and_duration_metrics: bool = False
    show_in_errors: bool = False
    latency_metric_name: str = "Latency"
    namespace: str = 'AWS/Lambda'
    dimension: str = "FunctionName"

    @validator('color', pre=True, always=True)
    def set_default_color(cls, color):
        if not color:
            color = random.choice(DASHBOARD_COLORS)
        return color


class ApiDashboardConfig(BaseModel):
    name: str
    label: str
    color: Optional[str]
    show_in_key_metrics: bool = False
    show_in_key_metric_errors: bool = True
    show_in_traffic_metrics: bool = False
    show_in_latency_and_duration_metrics: bool = False
    show_in_errors: bool = False
    latency_metric_name: str = "Duration"
    namespace: str = 'AWS/ApiGateway'
    dimension: str = "ApiName"

    @validator('color', pre=True, always=True)
    def set_default_color(cls, color):
        if not color:
            color = random.choice(DASHBOARD_COLORS)
        return color


class CustomKeyMetric(BaseModel):
    name: str
    label: str
    color: Optional[str]

    @validator('color', pre=True, always=True)
    def set_default_color(cls, color):
        if not color:
            color = random.choice(DASHBOARD_COLORS)
        return color


class BaseMetric(BaseModel):
    name: str
    label: str
    color: Optional[str]


class LambdaLatencyMetric(BaseMetric):
    namespace: str = 'AWS/Lambda'
    metric_name: str = "Duration"
    dimension: str = "FunctionName"


class ApiLatencyMetric(BaseMetric):
    namespace: str = 'AWS/ApiGateway'
    metric_name: str = "Latency"
    dimension: str = "ApiName"


class TextWidget:
    """
    Text widget component.
    """

    def __init__(self, markdown: str, width: int, height: int, x=0) -> None:
        self.__markdown = markdown
        self.__width = width
        self.__height = height
        self.__x = x

    def to_json(self):
        return {
            "type": "text",
            "width": self.__width,
            "height": self.__height,
            "x": self.__x,
            "properties": {
                "markdown": self.__markdown
            }
        }


class CloudwatchDashboardBuilder:
    """Builder class for AWS dashboard.
    """

    def __init__(self, service_name: str, lambdas_config: List[LambdaDashboardConfig],
                 apis_config: List[ApiDashboardConfig], custom_metrics: Optional[List[CustomKeyMetric]],
                 error_log_fields: List[str] = None) -> None:
        self.__service_name = service_name
        self.__region = DEPLOYMENT_REGION
        self.__key_metrics: list = []
        self.__traffic_metrics: list = []
        self.__latency_and_duration_metrics: list = []
        self.__errors_metrics: list = []
        self.__dashboard_body: dict = dict()
        self.__lambdas_config = lambdas_config
        self.__apis_config = apis_config
        self.__custom_metrics = custom_metrics
        self.__custom_metrics_namespace = os.environ.get('POWERTOOLS_METRICS_NAMESPACE', 'ecommerce.skx')
        self.__custom_metrics_service_name = os.environ.get('POWERTOOLS_SERVICE_NAME', 'service')
        self.__error_log_fields = ["timestamp", "level", "message as message"]
        if error_log_fields:
            self.__error_log_fields.extend(error_log_fields)

        self.__init_dashboard()

    def __init_dashboard(self) -> None:
        widgets = []
        self.__dashboard_body = {
            "start": "-PT6H",
            "periodOverride": "inherit",
            "widgets": widgets
        }
        try:
            self.__init_key_metrics()
            self.__init_traffic_metrics()
            self.__init_latency_and_duration_metrics()
            self.__init_errors_metrics()

            self.add_key_metrics()
            self.add_key_metrics_latency()
            self.add_key_metrics_errors()
            self.add_traffic_requests()
            self.add_latency_and_duration_metrics()
            self.add_lambda_errors()
            self.add_api_errors()
            self.add_error_logs()

            logger.info("Initialize the dashboard")
        except Exception as e:
            logger.error(e, exc_info=True)
            logger.info("failed to initialize the dashboard")

    def get_dashboard_body(self) -> dict:
        """
        Build the complete dashboard body.

        :return:
        """
        widgets = []
        widget_axis_x, widget_axis_y = [0, 0]
        max_width = 24

        widgets.extend(self.__key_metrics)
        widgets.extend(self.__traffic_metrics)
        widgets.extend(self.__latency_and_duration_metrics)
        widgets.extend(self.__errors_metrics)

        for widget in widgets:
            width = widget['width']
            height = widget['height']
            widget['x'] = widget_axis_x
            if widget.get("width") == max_width:
                widget_axis_y += height
            widget['y'] = widget_axis_y

            widget_axis_x += width
            if (widget_axis_x + width > max_width):
                widget_axis_x = 0
                widget_axis_y += height

        self.__dashboard_body['widgets'] = widgets
        return self.__dashboard_body

    def __init_key_metrics(self):
        text_widget = TextWidget(markdown="# Key Metrics", width=24, height=1)
        self.__key_metrics.append(text_widget.to_json())

    def __init_traffic_metrics(self) -> None:
        text_widget = TextWidget(markdown="# Traffic", width=24, height=1)
        self.__traffic_metrics.append(text_widget.to_json())

    def __init_latency_and_duration_metrics(self):
        text_widget = TextWidget(
            markdown="# Latency and Duration", width=24, height=1)
        self.__latency_and_duration_metrics.append(text_widget.to_json())

    def __init_errors_metrics(self) -> None:
        text_widget = TextWidget(markdown="# Errors", width=24, height=1)
        self.__errors_metrics.append(text_widget.to_json())

    def add_key_metrics(self) -> 'CloudwatchDashboardBuilder':
        all_metrics_json = []
        if self.__custom_metrics:
            for metric in self.__custom_metrics:
                all_metrics_json.append([
                    self.__custom_metrics_namespace,
                    metric.name,
                    "environment",
                    ENV,
                    "service",
                    self.__custom_metrics_service_name,
                    {
                        "color": metric.color,
                        "label": metric.label
                    }
                ])

            self.__key_metrics.append({
                "type": "metric",
                "width": 12,
                "height": 3,
                "properties": {
                    "view": "singleValue",
                    "period": 86400,
                    "stacked": False,
                    "stat": "Sum",
                    "region": self.__region,
                    "title": self.__service_name,
                    "metrics": all_metrics_json
                }
            })
        logger.info("Adding key metrics")
        return self

    def add_key_metrics_errors(self) -> 'CloudwatchDashboardBuilder':
        lambda_errors = []
        lambda_throttles = []
        api_errors = []
        lambda_ids = []
        api_ids = []

        for index, lambda_config in enumerate(self.__lambdas_config):
            if not lambda_config.show_in_key_metric_errors:
                continue

            lambda_ids.append(f"le{index}")
            lambda_errors.append([
                lambda_config.namespace,
                "Errors",
                lambda_config.dimension,
                lambda_config.name,
                {
                    "id": f"le{index}",
                    "visible": False
                }
            ])
            lambda_throttles.append([
                lambda_config.namespace,
                "Throttles",
                lambda_config.dimension,
                lambda_config.name,
                {
                    "id": f"lt{index}",
                    "visible": False
                }
            ])

        for index, api_config in enumerate(self.__apis_config):
            api_ids.append(f"ag{index}")
            api_errors.append([
                api_config.namespace,
                "5XXError",
                api_config.dimension,
                api_config.name,
                {
                    "id": f"ag{index}",
                    "visible": False
                }
            ])

        metrics = [
            [
                {
                    "expression": '+'.join(lambda_ids),
                    "label": "Lambda Errors",
                    "color": "#66bb6a"
                }
            ],
            [
                {
                    "expression": '+'.join(api_ids),
                    "label": "API Gateway 5XX",
                    "color": "#ef5350"
                }
            ],
        ]

        metrics.extend(lambda_errors)
        metrics.extend(lambda_throttles)
        metrics.extend(api_errors)

        self.__key_metrics.append({
            "type": "metric",
            "width": 12,
            "height": 3,
            "properties": {
                "metrics": metrics,
                "view": "singleValue",
                "period": 86400,
                "stacked": False,
                "stat": "Sum",
                "region": self.__region,
                "title": "Errors"
            }
        })
        return self

    def add_key_metrics_latency(self) -> 'CloudwatchDashboardBuilder':
        for lambda_config in self.__lambdas_config:
            if lambda_config.show_in_key_metrics:
                self.__key_metrics.append({
                    "height": 3,
                    "width": 12,
                    "type": "metric",
                    "properties": {
                        "metrics": [
                            ["AWS/Lambda", "Duration", "FunctionName", lambda_config.name,
                             {"color": "#9ccc65", "label": "p50"}],
                            ["...", {"stat": "p90", "color": "#ffee58", "label": "p90"}],
                            ["...", {"stat": "p99", "color": "#ef5350", "label": "p99"}]
                        ],
                        "view": "singleValue",
                        "period": 86400,
                        "stacked": False,
                        "stat": "p50",
                        "region": self.__region,
                        "title": lambda_config.label + " Latency"
                    }
                })
        return self

    def add_traffic_requests(self) -> 'CloudwatchDashboardBuilder':

        final_metrics = []
        for lambda_config in self.__lambdas_config:
            if lambda_config.show_in_traffic_metrics:
                final_metrics.append(
                    [lambda_config.namespace, "Invocations", lambda_config.dimension, lambda_config.name, {
                        "color": lambda_config.color, "label": lambda_config.label
                    }])
        if final_metrics:
            self.__traffic_metrics.append({
                "height": 6,
                "width": 12,
                "type": "metric",
                "properties": {
                    "metrics": final_metrics,
                    "view": "timeSeries",
                    "period": 60,
                    "stacked": False,
                    "stat": "Sum",
                    "region": self.__region,
                    "title": "Requests"
                }
            })
        return self

    def add_latency_and_duration_metrics(self) \
            -> 'CloudwatchDashboardBuilder':

        metrics: List[Union[LambdaLatencyMetric, ApiLatencyMetric]] = []
        for config in self.__lambdas_config:
            if config.show_in_latency_and_duration_metrics:
                metrics.append(LambdaLatencyMetric(name=config.name, label=config.label))

        for config in self.__apis_config:
            if config.show_in_latency_and_duration_metrics:
                metrics.append(ApiLatencyMetric(name=config.name, label=config.label))

        for metric in metrics:
            self.__latency_and_duration_metrics.append({
                "type": "metric",
                "width": 8,
                "height": 6,
                "properties": {
                    "metrics": [
                        [
                            metric.namespace,
                            metric.metric_name,
                            metric.dimension,
                            metric.name,
                            {
                                "color": "#9ccc65",
                                "label": "p50"
                            }
                        ],
                        [
                            "...",
                            {
                                "stat": "p90",
                                "color": "#ffee58",
                                "label": "p90"
                            }
                        ],
                        [
                            "...",
                            {
                                "stat": "p99",
                                "color": "#ef5350",
                                "label": "p99"
                            }
                        ]
                    ],
                    "view": "timeSeries",
                    "period": 60,
                    "stacked": False,
                    "stat": "p50",
                    "region": self.__region,
                    "title": metric.label + " " + metric.metric_name
                }
            })
        return self

    def add_error_logs(self) -> 'CloudwatchDashboardBuilder':
        query_fields = ", ".join(self.__error_log_fields)
        lambda_names = [f"SOURCE  '/aws/lambda/{config.name}'" for config in self.__lambdas_config]
        lambda_names_str = " | ".join(lambda_names)
        query = f" | fields {query_fields} \n| filter level == \"ERROR\"\n| sort @timestamp desc\n| limit 20\n"
        self.__errors_metrics.append({
            "type": "log",
            "width": 24,
            "height": 6,
            "properties": {
                "query": lambda_names_str + query,
                "region": self.__region,
                "stacked": False,
                "view": "table",
                "title": "Last 20 errors"
            }
        })
        return self

    def add_api_errors(self):
        metrics = []
        for config in self.__apis_config:
            metrics.append(
                ["AWS/ApiGateway", "4XXError", "ApiName", config.name, {"color": "#ffa726", "label": "4XX Errors"}])
            metrics.append(
                ["AWS/ApiGateway", "5XXError", "ApiName", config.name, {"color": "#ef5350", "label": "5XX Errors"}])
        self.__errors_metrics.append({
            "type": "metric",
            "width": 8,
            "height": 6,
            "properties": {
                "metrics": metrics,
                "view": "timeSeries",
                "period": 60,
                "stacked": False,
                "stat": "Sum",
                "region": self.__region,
                "title": "API Errors"
            }
        })

    def add_lambda_errors(self):
        metrics = []
        for config in self.__lambdas_config:
            metrics.append(["AWS/Lambda", "Errors", "FunctionName", config.name,
                            {"color": "#29b6f6", "label": config.label}])
        self.__errors_metrics.append({
            "type": "metric",
            "width": 8,
            "height": 6,
            "properties": {
                "metrics": metrics,
                "view": "timeSeries",
                "period": 60,
                "stacked": False,
                "stat": "Sum",
                "region": self.__region,
                "title": "Lambda Errors"
            }
        })
        return self

    @classmethod
    def full_dashboard_setup(cls, dashboard_name: str) -> 'CloudWatch':
        dashboard = CloudWatch(dashboard_name)
        return dashboard
