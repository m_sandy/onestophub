from jsonschema import Draft7Validator
from collections import defaultdict


class Validator:
    @staticmethod
    def validate(json_schema: dict, api_request: dict) -> dict:
        validator = Draft7Validator(json_schema)
        errors = sorted(validator.iter_errors(api_request), key=lambda e: e.path)
        api_errors = defaultdict(list)
        if errors:
            api_errors = defaultdict(list)
            for error in errors:
                full_path = ".".join([str(error_path) for error_path in error.path])
                if full_path:
                    api_errors[full_path].append(error.message)
                else:
                    api_errors["body"].append(error.message)
        return api_errors
