import json
from datetime import datetime
from typing import List
import botocore

from ecom.domain.event import EventSubscriber, EventDetails
from ecom.util.logging import Logger

logger = Logger()


class AWSEventSubscriber(EventSubscriber):

    def __init__(self, aws_event_bridge_client):
        self.__event_client = aws_event_bridge_client

    def process_event(self, event: EventDetails):
        logger.debug({'Event': event})
        self.process_events([event])

    def process_events(self, events: List[EventDetails], batch_size: int = 10) -> None:
        logger.info("Started processing events")
        logger.debug("Events batch size %s", batch_size)
        if events and len(events) > 0:
            event_batches = tuple(events[x:x + batch_size] for x in range(0, len(events), batch_size))
            for event_batch in event_batches:
                bus_events = []
                for event_detail in event_batch:
                    bus_events.append({
                        "Source": event_detail.source,
                        "EventBusName": event_detail.event_bus_name,
                        "DetailType": event_detail.type,
                        "Time": datetime.now(),
                        "Detail": json.dumps(event_detail.data),
                        "Resources": [event_detail.resource_id] if event_detail.resource_id else None
                    })
                    logger.debug("Event %s sent to %s ", event_detail.type, event_detail.event_bus_name)

                try:
                    put_events_response = self.__event_client.put_events(Entries=bus_events)
                    logger.debug("Events Posted to event bridge: %s", put_events_response)
                except botocore.exceptions.ClientError as error:
                    if error.response['Error']['Code'] == 'LimitExceededException':
                        logger.warning('API call limit exceeded; backing off and retrying...')
                        # TODO: User alternate event storage solution.
                    else:
                        # TODO: User alternate event storage solution.
                        logger.error(error)
                        raise error
        else:
            logger.error("No events available to post")

        logger.info("Completed processing events")
