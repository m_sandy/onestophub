import boto3
import json


class CloudWatch:
    """Base class for updated dashboard widgets"""

    def __init__(self, dashboard_name):
        """Name of dashboard to (re)create, list of account numbers to interrogate"""
        self.cw_client = boto3.client('cloudwatch')
        self.dashboard_name = dashboard_name
        self.dashboard = None

    def put_dashboard(self, dashboard_body):
        """Puts the updated dashboard into CloudWatch"""
        results = self.cw_client.put_dashboard(
            DashboardName=self.dashboard_name,
            DashboardBody=json.dumps(dashboard_body))
        print(results)

    def delete_dashboard(self):
        """Delete dashboard from CloudWatch"""
        self.cw_client.delete_dashboard([self.dashboard_name])
