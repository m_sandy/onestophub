import os

DEPLOYMENT_REGION = os.getenv('AWS_REGION', 'us-west-1')
ENV = os.getenv('ENVIRONMENT', 'dev')
