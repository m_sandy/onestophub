from typing import Optional

from ecom.domain.repository.parameter_repository import ParameterRepository


class ParameterService:

    def __init__(self, parameter_repository: ParameterRepository) -> None:
        self.__parameter_repository = parameter_repository

    def get_parameter(self, name: str) -> Optional[str]:
        """Get the configuration parameter

        Args:
            name (str): Name of the paramter

        Returns:
            Optional[str]: Value configured
        """
        return self.__parameter_repository.get_parameter(name)
