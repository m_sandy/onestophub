from abc import ABC, abstractmethod
from typing import Optional

class ParameterRepository(ABC):

    @abstractmethod
    def get_parameter(self, name: str) -> Optional[str]:
        """Get paramter by paramter name

        Args:
            name (str): Name of the paramter.

        Returns:
            Optional[str]: String value of the configured paramter.
        """
        pass



