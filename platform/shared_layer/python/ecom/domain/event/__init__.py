from .event_subscriber import EventSubscriber, EventDetails

__all__ = [
    'EventSubscriber',
    'EventDetails',
]
