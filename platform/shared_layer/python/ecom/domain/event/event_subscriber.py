import os
from pydantic import BaseModel, validator
from typing import Dict, Optional


class EventDetails(BaseModel):
    """
    Event Bus name / Topic name
    """
    event_bus_name: Optional[str]
    """
    Resource Ids, can be DB record ids (order_id, product_id, ...)
    """
    resource_id: Optional[str]

    """
    Source name can be used by the consumer to read only messages form certain sources.
    Ex: orders, products, ...
    """
    source: str

    """
    Type of event, Ex: OrderUpdate, ProductCreated, .... 
    """
    type: str

    """
    Full event details that can be utilized by the message consumer.
    Ex: order details, product details, ....
    """
    data: Optional[Dict]

    @validator('event_bus_name', pre=True, always=True)
    def set_default_color(cls, event_bus_name):
        if not event_bus_name:
            event_bus_name = os.environ.get('EVENT_BUS_NAME')
        return event_bus_name


class EventSubscriber:
    PLATFORM_TOPIC_NAME = 'platform_topic'

    def process_event(self, event_details: EventDetails): ...
