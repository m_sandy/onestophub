import boto3
import pinject
import sys
from pubsub import pub

from ecom.adapter.aws.parameter_repository_adapter import ParamterRepositoryAdapter
from ecom.domain.service.parameter_service import ParameterService
from ecom.adapter.aws.aws_event_subscriber import AWSEventSubscriber
from ecom.domain.event.event_subscriber import EventSubscriber


class EcomBindingSpec(pinject.BindingSpec):

    def configure(self, bind):
        bind('parameter_repository', to_class=ParamterRepositoryAdapter)
        bind('parameter_service', to_class=ParameterService)
        bind('platform_event_subscriber', to_class=AWSEventSubscriber)

    def provide_aws_parameter_client(self):
        return boto3.client('ssm')

    def provide_aws_event_bridge_client(self):
        return boto3.client('events')


def create_container():
    obj_graph = pinject.new_object_graph(modules=[sys.modules[__name__]], binding_specs=[EcomBindingSpec()])
    return obj_graph


core_container = create_container()

# Register Platform Event listener
platform_event_listener = core_container.provide(AWSEventSubscriber)
listener = pub.subscribe(platform_event_listener.process_events, EventSubscriber.PLATFORM_TOPIC_NAME)
