import pytest
import os
from unittest import mock

os.environ['PRODUCTS_API_URL'] = "https://products_api_url.com"
os.environ['POWERTOOLS_METRICS_NAMESPACE'] = "ecom-test"

@pytest.fixture(autouse=True)
@mock.patch('skechers_orders.containers.BotoAWSRequestsAuth.get_aws_request_headers_handler')
def mock_auth(get_aws_request_headers_handler):
    get_aws_request_headers_handler.side_effect = {}
