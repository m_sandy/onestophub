import os
import requests
from requests.auth import AuthBase
from typing import Tuple

from ecom.util.logging.logger import Logger

logger = Logger()

PRODUCTS_API_URL = os.environ.get("PRODUCTS_API_URL")


class OrderService:

    def __init__(self, products_api_auth: AuthBase) -> None:
        self.__products_api_auth = products_api_auth

    def validate_products(self, order: dict) -> Tuple[bool, str]:
        """Validate the products in the order
        Args:
            order (dict): Order Payload

        Returns:
            Tuple[bool, str]: Returns bool val true if request valid and api response.
        """
        # Send a POST request
        response = requests.post(
            PRODUCTS_API_URL + "/backend/validate",
            json={"products": order["products"]},
            auth=self.__products_api_auth
        )

        logger.debug({
            "message": "Response received from products",
            "body": response.json()
        })

        body = response.json()
        return (len(body.get("products", [])) == 0, body.get("message", ""))

