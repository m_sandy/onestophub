"""Orders containers module."""

import os
import sys
import pinject
import requests
import boto3
from pinject.object_graph import ObjectGraph
from urllib.parse import urlparse
from aws_requests_auth.boto_utils import BotoAWSRequestsAuth

from ecom.containers import EcomBindingSpec

PRODUCTS_API_URL = os.environ.get("PRODUCTS_API_URL")

class OrdersBindingSpec(pinject.BindingSpec):
    """
    Binding spec order service classes.
    """
    

    def configure(self, bind): pass

    def provide_products_api_auth(self) -> requests.auth.AuthBase:
        # Gather the domain name and AWS region
        iam_auth = None
        try:
            url = urlparse(PRODUCTS_API_URL)
            region = boto3.session.Session().region_name
            # Create the signature helper
            iam_auth = BotoAWSRequestsAuth(aws_host=url.netloc,
                                        aws_region=region,
                                        aws_service='execute-api')
        except:
            pass
        return iam_auth


def create_container() -> ObjectGraph:
    obj_graph = pinject.new_object_graph(modules=[sys.modules[__name__]],
                                         binding_specs=[EcomBindingSpec(), OrdersBindingSpec()])
    return obj_graph


order_container: ObjectGraph = create_container()
