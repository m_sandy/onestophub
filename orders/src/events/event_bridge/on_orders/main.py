"""
OnPackageCreated Function
"""

from collections import defaultdict
import os
from typing import List, Optional
from aws_lambda_powertools.tracing import Tracer  # pylint: disable=import-error
from aws_lambda_powertools.logging.logger import Logger  # pylint: disable=import-error
from aws_lambda_powertools import Metrics  # pylint: disable=import-error
from aws_lambda_powertools.metrics import MetricUnit  # pylint: disable=import-error

ENVIRONMENT = os.environ["ENVIRONMENT"]

logger = Logger()  # pylint: disable=invalid-name
tracer = Tracer()  # pylint: disable=invalid-name
metrics = Metrics(namespace="ecommerce.orders")  # pylint: disable=invalid-name


@metrics.log_metrics
@logger.inject_lambda_context
@tracer.capture_lambda_handler
def handler(event, _):
    """
    Lambda handler
    """
    logger.info(event)
    order_ids = event["resources"]

    metrics_data = defaultdict(int)

    for order_id in order_ids:
        logger.info({
            "message": "Got event of type {} from {} for order {}".format(event["detail-type"], event["source"],
                                                                          order_id),
            "source": event["source"],
            "eventType": event["detail-type"],
            "orderId": order_id
        })
        tracer.put_annotation("orderId", order_id)
        if event["source"] == "ecommerce.orders":
            if event["detail-type"] == "OrderReceived":
                if event["detail"]["products"][0]["price"] == 22:
                    metrics_data["orderFailed"] += 1
                    logger.error("Failed to process the order {}".format(order_id))
                else:
                    metrics_data["orderProcessed"] += 1
        else:
            logger.warning({
                "message": "Unknown source {} for order {}".format(event["source"], order_id),
                "source": event["source"],
                "eventType": event["detail-type"],
                "orderId": order_id
            })

    # Add custom metrics
    metrics.add_dimension(name="environment", value=ENVIRONMENT)
    for key, value in metrics_data.items():
        metrics.add_metric(name=key, unit=MetricUnit.Count, value=value)
