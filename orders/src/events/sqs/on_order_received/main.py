"""
OnPackageCreated Function
"""

import os
from aws_lambda_powertools.tracing import Tracer  # pylint: disable=import-error
from aws_lambda_powertools.logging.logger import Logger  # pylint: disable=import-error
from aws_lambda_powertools import Metrics  # pylint: disable=import-error

ENVIRONMENT = os.environ["ENVIRONMENT"]

logger = Logger()  # pylint: disable=invalid-name
tracer = Tracer()  # pylint: disable=invalid-name
metrics = Metrics(namespace="ecommerce.orders")  # pylint: disable=invalid-name


@metrics.log_metrics
@logger.inject_lambda_context
@tracer.capture_lambda_handler
def handler(event, _):
    """
    Lambda handler
    """
    logger.info(event)
    for record in event['Records']:
        payload = record["body"]
        logger.info(str(payload))
