import os
from crhelper import CfnResource

from ecom.util.logging import Logger
from ecom.adapter.aws.cloudwatch_adapter import CloudWatch
from ecom.util.aws.cloudwatch_metrics import CloudwatchDashboardBuilder, LambdaDashboardConfig, ApiDashboardConfig, \
    CustomKeyMetric

helper = CfnResource()
logger = Logger()
DASHBOARD_NAME = os.getenv('DASHBOARD_NAME')


@helper.create
@helper.update
def create_or_update(event, _):
    try:
        create_order_function = os.getenv('CREATE_ORDER_FUNCTION')
        on_events_function = os.getenv('ON_EVENTS_FUNCTION')
        api_name = os.getenv('API_NAME')

        lambdas_config = [
            LambdaDashboardConfig(
                name=create_order_function,
                label='Create Order',
                show_in_key_metrics=True,
                show_in_traffic_metrics=True,
                show_in_latency_and_duration_metrics=True,
                show_in_errors=True
            ),
            LambdaDashboardConfig(
                name=on_events_function,
                label='On Events',
                show_in_errors=True
            )
        ]
        apis_config = [
            ApiDashboardConfig(
                name=api_name,
                label='Orders API',
                show_in_key_metrics=True,
                show_in_traffic_metrics=True,
                show_in_latency_and_duration_metrics=True,
                show_in_errors=True
            )]
        custom_metrics = [CustomKeyMetric(name="orderCreated", label="Orders Created"),
                          CustomKeyMetric(name="orderFailed", label="Orders Failed")]

        """Entrypoint for the Lambda function"""
        builder = CloudwatchDashboardBuilder('Orders', lambdas_config, apis_config, custom_metrics)
        dashboard_body = builder.get_dashboard_body()

        logger.info({"dashboard_body": dashboard_body})

        cw = CloudWatch(DASHBOARD_NAME)
        cw.put_dashboard(dashboard_body)
        logger.info("Created dashboard")
    except Exception as e:
        logger.error(e, exc_info=True)
        logger.info("failed")


@helper.delete
def no_op(_, __):
    try:
        cw = CloudWatch(os.getenv('DASHBOARD_NAME'))
        cw.delete_dashboard()
    except Exception as e:
        logger.error(e, exc_info=True)
        print("Dashboard delete failed")


def handler(event, context):
    logger.info(event)
    helper(event, context)
