"""
CreateOrderFunction
"""

import asyncio
import concurrent
import datetime
import json
import os
from typing import List
import uuid
from pubsub import pub

from aws_lambda_powertools import Metrics
from aws_lambda_powertools.metrics import MetricUnit
from aws_lambda_powertools.tracing import Tracer

from ecom.util.logging.logger import Logger
from ecom.apigateway import response
from ecom.domain.constants import ENV
from ecom.util import Validator
from ecom.domain.event import EventSubscriber, EventDetails
from skechers_orders.containers import order_container
from skechers_orders.domain.service import OrderService

SCHEMA_FILE = os.path.join(os.path.dirname(__file__), "schema.json")

logger = Logger()
tracer = Tracer()
metrics = Metrics()
order_service: OrderService = order_container.provide(OrderService)

with open(SCHEMA_FILE) as fp:
    schema = json.load(fp)


async def validate(order: dict) -> List[str]:
    """
    Returns a list of error messages
    """
    error_msgs = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        futures = [
            executor.submit(order_service.validate_products, order)
        ]
        for future in concurrent.futures.as_completed(futures):
            valid, error_msg = future.result()
            if not valid:
                error_msgs.append(error_msg)

    if error_msgs:
        logger.info({
            "message": "Validation errors for order",
            "order": order,
            "errors": error_msgs
        })

    return error_msgs


def cleanup_products(products: List[dict]) -> List[dict]:
    """
    Cleanup products
    """

    return [{
        "productId": product["productId"],
        "name": product["name"],
        "package": product["package"],
        "price": product["price"],
        "quantity": product.get("quantity", 1)
    } for product in products]


def inject_order_fields(order: dict) -> dict:
    """
    Inject fields into the order and return the order
    """

    now = datetime.datetime.now()

    order["orderId"] = str(uuid.uuid4())
    order["status"] = "NEW"
    order["createdDate"] = now.isoformat()
    order["modifiedDate"] = now.isoformat()
    order["total"] = sum([p["price"] * p.get("quantity", 1)
                          for p in order["products"]]) + order["deliveryPrice"]

    return order


@metrics.log_metrics(raise_on_empty_metrics=False)
@logger.inject_lambda_context
@tracer.capture_lambda_handler
def handler(event, _):
    """
    Lambda function handler
    """
    event_body = json.loads(event.get('body'))
    # Basic checks on the event
    for key in ["orderId", "userId"]:
        if key not in event_body:
            return response({
                "success": False,
                "message": "Invalid event",
                "errors": ["Missing {} in event".format(key)]
            }, status_code=400)

    # Inject userId into the order
    order = event_body
    # order["userId"] = event["userId"]

    logger.info({
        "message": "Order {} request received".format(order["orderId"]),
        "orderId": order["orderId"],
        "order": order
    })

    # Validate the schema of the order
    validation_errors = Validator.validate(schema, order)
    if validation_errors:
        logger.info(validation_errors)
        return response({
            "success": False,
            "message": "JSON Schema validation error",
            "errors": validation_errors
        }, status_code=400)

    # # Cleanup products
    order["products"] = cleanup_products(order["products"])

    # Inject fields in the order
    order = inject_order_fields(order)

    # Validate the order against other services
    error_msgs = asyncio.run(validate(order))
    if len(error_msgs) > 0:
        return response({
            "success": False,
            "message": "Validation errors",
            "errors": error_msgs
        }, status_code=400)

    # Log
    tracer.put_annotation("orderId", order["orderId"])
    logger.info({
        "message": "Order {} created.".format(order["orderId"]),
        "orderId": order["orderId"]
    })
    logger.debug({
        "message": "Order {} created".format(order["orderId"]),
        "orderId": order["orderId"],
        "order": order
    })

    pub.sendMessage(EventSubscriber.PLATFORM_TOPIC_NAME, events=[EventDetails(
        source="ecommerce.orders",
        type="OrderReceived",
        data=order,
        resource_id=order["orderId"]
    )])

    # Add custom metrics
    metrics.add_dimension(name="environment", value=ENV)
    metrics.add_metric(name="orderCreated", unit=MetricUnit.Count, value=1)
    metrics.add_metric(name="orderCreatedTotal", unit=MetricUnit.Count, value=order["total"])

    return response({
        "success": True,
        "order": order,
        "message": "Order created"
    }, status_code=201)
