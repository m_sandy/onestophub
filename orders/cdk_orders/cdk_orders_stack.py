from aws_cdk import (
    Duration,
    aws_sqs as sqs, aws_ssm as ssm,
    CfnParameter,
    aws_apigateway as apigateway,
    aws_cloudwatch as cloudwatch,
    aws_lambda_event_sources as lambda_event_source,
    aws_iam as iam,
    aws_events as events,
    aws_events_targets as targets,
)
from cdk_watchful import Watchful
from constructs import Construct
from skx_cdk.skx_api_stack import SkxApiStack
from skx_cdk.construct.lambda_construct import SkxLambda
from skx_cdk.construct.api_gateway_construct import SkxApiGateway


class CdkOrdersStack(SkxApiStack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        stack_name = self.stack_name

        # Stack parameters
        product_api_url = CfnParameter(self, "ProductsApiUrl", type="AWS::SSM::Parameter::Value<String>",
                                       description="Product API Url")

        product_api_arn = CfnParameter(self, "ProductsApiArn", type="AWS::SSM::Parameter::Value<String>",
                                       description="Product API Arn")

        # -------------- Lambda Setup -------------------------

        # -- Create Order Function --
        create_order_lambda_construct = SkxLambda(self, "CreateOrderFunction",
                                                  code="src/api/create_order/",
                                                  enable_dlq=True,
                                                  ssm_name_for_arn="create-order")
        create_order_lambda_construct.handler.add_environment("PRODUCTS_API_URL", product_api_url.value_as_string)

        # Grant permission to send messages to Event Bridge.
        event_bridge_write = iam.PolicyStatement(
            actions=["events:PutEvents"],
            resources=[self.skx_event_bus_arn]
        )

        # Grant permission to Call Product Validate POST API.
        product_api_post = iam.PolicyStatement(
            actions=["execute-api:Invoke"],
            resources=[f"{product_api_arn.value_as_string}/POST/*"]
        )

        create_order_lambda_construct.handler.role.attach_inline_policy(
            iam.Policy(self, "CreateOrderPolicy", statements=[event_bridge_write, product_api_post])
        )

        # -- Get Order Function --
        SkxLambda(self, "GetOrderFunction", code="src/api/get_order/")

        received_orders_lambda = SkxLambda(self, "OnEventsFunction", code="src/events/event_bridge/on_orders/")

        queue_orders_process = SkxLambda(self, "OnOrderQueueFunction", code="src/events/sqs/on_order_received/")

        api_lambda_invoke_role = self.get_api_lambda_invoke_role([
            self.skx_authorizer,
            create_order_lambda_construct.handler.function_arn
        ])

        # ------- API Setup ------------------
        api_name = f"{stack_name}-api"
        skx_apigateway_construct = SkxApiGateway(self, api_name, base_path="v1/orders",
                                                 role=api_lambda_invoke_role.role_arn)
        api = skx_apigateway_construct.api
        # ------- Lambda API Mapping ------------------
        skx_apigateway_construct.api.root.add_method("POST",
                                                     apigateway.LambdaIntegration(
                                                         create_order_lambda_construct.handler),
                                                     authorization_type=apigateway.AuthorizationType.CUSTOM,
                                                     ).node.find_child('Resource'). \
            add_property_override('AuthorizerId',
                                  skx_apigateway_construct.authorizer.ref)

        # ----- SQS Lambda Integrations ------------------------
        orders_received_queue = sqs.Queue(
            self, "CdkOrdersQueue",
            visibility_timeout=Duration.seconds(self.get_env_mapping("QueueMessageVisibilityTimeout", 900)),
            retention_period=Duration.seconds(self.get_env_mapping("QueueMessageRetention", 172800)),
            removal_policy=None
        )
        orders_received_queue_event_source = lambda_event_source.SqsEventSource(orders_received_queue)
        queue_orders_process.handler.add_event_source(orders_received_queue_event_source)

        # Event Bridge lambda Integration ----------------
        platform_event_bus = events.EventBus.from_event_bus_arn(self, "PlatformBus", self.skx_event_bus_arn)
        order_received_rule = events.Rule(self, 'OrderReceivedRule',
                                          event_bus=platform_event_bus,
                                          description='Rule to listen for Orders in received state',
                                          event_pattern=events.EventPattern(source=['ecommerce.orders'],
                                                                            detail_type=["OrderReceived"]
                                                                            ))

        order_received_rule.add_target(targets.LambdaFunction(handler=received_orders_lambda.handler))
        order_received_rule.add_target(targets.SqsQueue(queue=orders_received_queue))

        # --------------- Monitoring
        watchful = Watchful(self, "monitoring", alarm_sns=self.skx_alarm_topic, dashboard_name=stack_name)
        watchful.add_widgets(cloudwatch.TextWidget(markdown="# Key Metrics", width=24, height=1),
                             cloudwatch.SingleValueWidget(
                                 metrics=[cloudwatch.Metric(
                                     metric_name="orderCreated",
                                     label="Orders Created",
                                     namespace=f"ecommerce.{self.skx_service_name}",
                                     dimensions_map={
                                         "environment": self.skx_environment,
                                         "service": self.skx_service_name,
                                     }
                                 )]
                             ))
        watchful.watch_api_gateway(title="Order API", rest_api=api)
        watchful.add_widgets(cloudwatch.TextWidget(markdown="# Lambda Metrics", width=24, height=1))
        watchful.watch_lambda_function("Create Order Function", create_order_lambda_construct.handler)

    def env_mapping(self) -> dict:
        env_mapping = {
            "dev": {
                "CreateOrderFunctionProvisionedConcurrency": 1,
            },
            "test": {
                "CreateOrderFunctionProvisionedConcurrency": 1,
            },
            "prod": {
                "CreateOrderFunctionProvisionedConcurrency": 1,
            },
        }
        return env_mapping
