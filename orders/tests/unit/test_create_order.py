import asyncio
import copy
import json
import pytest
import requests_mock
from unittest import mock
from fixtures import context, lambda_module, get_order, get_product  # pylint: disable=import-error

from skechers_orders.containers import order_container
from skechers_orders.domain.service import OrderService

order_service: OrderService = order_container.provide(OrderService)
PRODUCTS_API_URL = 'https://products_api_url.com'

lambda_module = pytest.fixture(scope="module", params=[{
    "function_dir": "api/create_order",
    "module_name": "main",
    "environ": {
        "ENVIRONMENT": "test",
        "PRODUCTS_API_URL": PRODUCTS_API_URL,
        "POWERTOOLS_TRACE_DISABLED": "true"
    }
}])(lambda_module)
context = pytest.fixture(context)


@pytest.fixture
def order(get_order):
    """
    Order fixture
    """

    order = get_order()
    return {k: order[k] for k in [
        "orderId", "userId", "products", "address", "deliveryPrice", "paymentToken"
    ]}


@pytest.fixture
def complete_order(get_order):
    """
    Complete order fixture
    """

    return get_order()


def test_inject_order_fields(lambda_module, order):
    """
    Test inject_order_fields()
    """

    new_order = lambda_module.inject_order_fields(order)

    assert "orderId" in new_order
    assert "createdDate" in new_order
    assert "modifiedDate" in new_order
    assert "total" in new_order
    assert new_order["total"] == sum([p["price"] * p.get("quantity", 1) for p in order["products"]]) + order[
        "deliveryPrice"]


def test_validate_products(order):
    """
    Test validate_products()
    """

    url = f"{PRODUCTS_API_URL}/backend/validate"

    with requests_mock.Mocker() as m:
        m.post(url, text=json.dumps({"message": "All products are valid"}))

        valid, error_msg = order_service.validate_products(order)

    print(valid, error_msg)

    assert m.called
    assert m.call_count == 1
    assert m.request_history[0].method == "POST"
    assert m.request_history[0].url == url
    assert valid == True


def test_validate_products_fail(order):
    """
    Test validate_products() failing
    """

    url = f"{PRODUCTS_API_URL}/backend/validate"

    with requests_mock.Mocker() as m:
        m.post(
            url,
            text=json.dumps({"message": "Something is wrong", "products": order["products"]}),
            status_code=200
        )

        valid, error_msg = order_service.validate_products(order)

    assert m.called
    assert m.call_count == 1
    assert m.request_history[0].method == "POST"
    assert m.request_history[0].url == url
    assert valid == False
    assert error_msg == "Something is wrong"


@mock.patch('skechers_orders.domain.service.OrderService.validate_products')
def test_validate(validate_products, lambda_module, order):
    """
    Test validate()
    """
    validate_products.return_value = (True, "")

    error_msgs = asyncio.run(lambda_module.validate(order))
    assert len(error_msgs) == 0


@mock.patch('skechers_orders.domain.service.OrderService.validate_products')
def test_validate_fail(validate_products, lambda_module, order):
    """
    Test validate() with failures
    """
    validate_products.return_value = (False, "Something is wrong")

    error_msgs = asyncio.run(lambda_module.validate(order))
    assert len(error_msgs) == 1


@mock.patch('skechers_orders.domain.service.OrderService.validate_products')
@mock.patch('skechers_orders.domain.service.OrderService.validate_products')
def test_handler(validate_products, lambda_module, context, order):
    """
    Test handler()
    """
    validate_products.return_value = (True, "")
    user_id = order["userId"]
    order = copy.deepcopy(order)

    response = lambda_module.handler({
        "body": json.dumps(order)
    }, context)

    response = json.loads(response.get("body"))
    assert response["success"] is True
    assert len(response.get("errors", [])) == 0
    assert "order" in response


@mock.patch('skechers_orders.domain.service.OrderService.validate_products')
def test_handler_wrong_event(validate_products, lambda_module, context, order):
    """
    Test handler() with an incorrect event
    """
    validate_products.return_value = (True, "")
    del order["orderId"]

    response = lambda_module.handler({
        "body": json.dumps(order)
    }, context)

    response = json.loads(response.get("body"))
    assert response["success"] is False
    assert len(response.get("errors", [])) > 0


def test_handler_schema_validation_failure(monkeypatch, lambda_module, context, order):
    """
    Test handler() with failing validation
    """
    user_id = order["userId"]
    order = copy.deepcopy(order)
    del order["products"][0]["package"]["weight"]

    response = lambda_module.handler({
        "body": json.dumps(order)
    }, context)

    response = json.loads(response.get("body"))
    assert response["success"] is False
    # assert len(response.get("errors", [])) > 0
