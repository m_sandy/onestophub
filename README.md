Onestophub Platform
===================

## Getting started
To install the necessary tools and deploy this in your own AWS account, see the [getting started](docs/getting_started.md) guide in the documentation section.


## Architecture
### High-level architecture
<p>
  <img src="docs/images/Onestophub-High-level-architecture.png" alt="High-level architecture diagram"/>
</p>


### Technologies used
__Communication/Messaging__:

* [Amazon API Gateway](https://aws.amazon.com/api-gateway/) for service-to-service synchronous communication (request/response).
* [Amazon EventBridge](https://aws.amazon.com/eventbridge/) for service-to-service asynchronous communication (emitting and reacting to events).


__Compute__:

* [AWS Lambda](https://aws.amazon.com/lambda/) as serverless compute either behind APIs or to react to asynchronous events.


__Storage__:


__CI/CD__:

* Jenkins

__Monitoring__:
* [Amazon CloudWatch](https://aws.amazon.com/cloudwatch/) for metrics, dashboards, log aggregation.


### Backend services
| Services                              | Description                                                    |
|---------------------------------------|----------------------------------------------------------------|
| [loyalty](loyalty/)                   | Provides loyalty management. |


### Infrastructure services

|  Services  | Description                               |
|------------|-------------------------------------------|
| [platform](platform/) | Core platform resources for deploying backend services. |

### Shared resources
| Name       | Description                               |
|------------|-------------------------------------------|
| [docs](docs/) | Documentation application for all services. |
| [shared](shared/) | Shared resources accessible for all services, such as common CloudFormation templates and OpenAPI schemas. |
| [tools](tools/) | Tools used to build services.             |


## Documentation

See the [docs](docs/) folder for the documentation.


