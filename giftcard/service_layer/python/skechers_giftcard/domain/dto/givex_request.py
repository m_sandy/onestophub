from pydantic import BaseModel
from typing import Optional


class BaseGiveXRequest(BaseModel):
    user_name: Optional[str]
    password: Optional[str]


class GiveXHistoryRequest(BaseGiveXRequest):
    type: str = 'Certificate'
    card_number: str


class AdjustBalanceRequest(BaseGiveXRequest):
    name: str
