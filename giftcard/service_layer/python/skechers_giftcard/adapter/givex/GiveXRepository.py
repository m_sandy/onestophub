from skechers_giftcard.domain.repository.giftcard_repository import GiftCardRepository
from skechers_giftcard.domain.dto.givex_request import GiveXHistoryRequest


class GivexRepository(GiftCardRepository):

    def get_history(self, card_number: str):
        # give x APi

        request_parameter = '1111'
        request = GiveXHistoryRequest(
            card_number=request_parameter
        )
        request.dict().values()

        # call API
