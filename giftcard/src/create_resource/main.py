"""
CreateResourceFunction
"""

from ecom.util.logging.logger import Logger
from ecom.apigateway import response

logger = Logger()


@logger.inject_lambda_context
def handler(event, _):
    """
    Lambda function handler
    """
    return response({
        "success": True,
        "message": "Resource created"
    }, status_code=201)
