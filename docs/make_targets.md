Make targets
============

## Project-level targets

These targets are part of the [Makefile at the root of this repository](../Makefile).
* __setup__: Configure the development environment.
* __activate__: Activate the pyenv virtual environment for Python.
* __requirements__: Install python dependencies for this project.
* __npm-install__: Install node dependencies for this project.



## Service-level target
These targets should be defined in the Makefile of each individual service. You can run the target by running `make $TARGET-$SERVICE` in the root of this project, or `make $TARGET` to run it against all services, e.g. `make tests-unit-all`.

* __artifacts__: Create a zip file containing the template and artifacts for the CI/CD pipeline.
* __build__: Build the resources to deploy the service, such as Lambda functions, OpenAPI specification, CloudFormation templates, etc.
* __check-deps__: Checks if the dependencies of this service are deployed in the target environment.
* __clean__: Remove the build artifacts for the service.
* __deploy__: Deploy/update the service on AWS, usually create/update the CloudFormation stack.
* __lint__: Run linting checks on source code, CloudFormation template, etc.
* __package__: Package and store artifacts in an S3 bucket in preparation for deployment.
* __teardown__: Tear down resources for that services on AWS, usually delete the CloudFormation stack.
* __tests-unit__: Run unit tests locally.