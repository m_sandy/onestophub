Service structure
=================

At minimum, a service should contain two files: __Makefile__ and __metadata.yaml__. The Makefile contains instructions to build, package, deploy, test a given service using [GNU make](https://www.gnu.org/software/make/). The metadata.yaml file contains information such as dependencies and CloudFormation parameters.

When using one of the [default Makefiles](../shared/makefiles/), there might be other files which will be described throughout this document.

For the list of targets for Makefile, please refer to the [Make targets](make_targets.md) page.

## metadata.yaml

The __metadata.yaml__ files contains information such as its name, dependencies and feature flags. See the schema definition to know what you can use for your service. Here's an example of a metadata file:

```yaml
name: my-service

# These will be used to check whether all dependencies for a service are
# deployed and check if there are any circular dependency. This allows to
# redeploy the entire infrastructure from scratch.
dependencies:
  - platform
  - loyalty

# This section is used for service discovery. See the 'Service discovery' page in the documentation for more information.
parameters:
  EventBusName: /ecommerce/{Environment}/platform/event-bus/name
  ProductsApiArn: /ecommerce/{Environment}/products/api/arn
  ProductsApiUrl: /ecommerce/{Environment}/products/api/url


# Boolean flags regarding a specific service. For example, the pipeline service
# does not support environments, or some services might not support tests.
# This section is optional and the values provided there are the default values.
flags:
  environment: true
  skip-tests: false
```

## template.yaml

The __template.yaml__ file is the CloudFormation template that defines the resources that are part of the service.

## resources/ folder
This folder contains resource files such as OpenAPI document for API Gateway REST APIs or EventBridge event schemas, nested CloudFormation templates, etc.


