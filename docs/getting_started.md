Getting started
===============

If you are experience an issue while setting up this project, please take a look at the [Common issues](common_issues.md) section of the documentation. If you cannot find a solution to your problem there, please [create a ticket](https://github.com/aws-samples/aws-serverless-ecommerce-platform/issues/new) explaining the issue that you are experiencing and the steps to reproduce it.

## Setup the development environment
To set up the development environment, you will need to install __pyenv__ on your computer. You can find installation instruction at [https://github.com/pyenv/pyenv-installer](https://github.com/pyenv/pyenv-installer). Please make sure that you have the [required tools and libraries](https://github.com/pyenv/pyenv/wiki/Common-build-problems) installed in your environment.

You will also need [Node](https://nodejs.org/en/) version 12 or greater, requird node can be installed using [https://github.com/nvm-sh/nvm](NVM).
`nvm install 12`

After __pyenv__ is installed, you can run `make setup` to configure the Python environment for this project, including development tools and dependencies.
 
[jq](https://stedolan.github.io/jq/) and __md5sum__. __md5sum__ is not available by default on MacOS but can be installed through the [coreutils formula in homebrew](https://formulae.brew.sh/formula/coreutils)


## Deploy the infrastructure on AWS
If you want to deploy only a specific service and its dependencies, you can use the command `make deps-${SERVICE}`.

These commands will lint, build, run unit tests, package, deploy and run integration tests on the services.

## Useful commands
All the following commands can be run without the service name (e.g. `make tests-integ` to run integration tests for all services).

* __`make create-service name=${SERVICE NAME}`__: Create a new webservice with one default lambda function and API Gateway.
* __`make build-${SERVICE}`__: Build the project.
* __`make bpdeploy-${SERVICE}`__: Build, package and deploy the service.
* __`make poetry-update`__: Update the poetry lock file after making change to the poetry `project.toml` file.
* __`make poetry-install`__: Install all the required python libraries locally to be available for the IDE.

## Creating or modifying a service
To read how you can create a new service or modify an existing one, please read the [service structure documentation](service_structure.md).

## Architecture



