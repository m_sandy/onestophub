[Original Article with Install steps](https://dev.to/sergej_brazdeikis/install-docker-on-mac-m1-without-docker-desktop-k6o), added all the same steps below for quick ref.

## Docker on Mac with no Docker Desktop
`brew install docker docker-compose`

## Docker Compose on Mac without Docker Desktop
After installing the Docker, you will see the message.

`Compose is now a Docker plugin. For Docker to find this plugin, symlink it:`<br>
`mkdir -p ~/.docker/cli-plugins`<br>
`ln -sfn /opt/homebrew/opt/docker-compose/bin/docker-compose ~/.docker/cli-plugins/docker-compose`

Just execute the above, and we did not finish yet!

## Free Replacement Docker Desktop
Docker Desktop installs Docker Engine, which can run only on Linux. So far, we have installed only CLI tools. For images to run, we need an environment.

Rancher Desktop replaces Docker Desktop! Happily to Rancher Desktop v1.0.0 was just released with M1 chip compatibility and solved this problem for us! <br>
https://rancherdesktop.io/

#### For any install issues please refer to [Common issues](common_issues.md) page. 