# Common issues

### pyenv Setup
`export PATH="$(pyenv root)/shims:$HOME/.poetry/bin:$PATH"`

### md5sum: command not found
`brew install md5sha1sum`

### aws: command not found
[AWS Client install](https://docs.aws.amazon.com/cli/v1/userguide/install-macos.html)

### rancherdesktop Install error
[Rancher Desktop fails to start on MacOS](https://github.com/rancher-sandbox/rancher-desktop/issues/1815)
`sudo chmod 775 /private/var/run/rancher-desktop-lima`

