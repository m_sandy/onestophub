FROM python:3.9-buster
ARG USER_UID=1001
ARG USER_GID=$USER_UID
ARG USERNAME=onestophubuser

# Add and use user onestophubuser
RUN groupadd --gid $USER_GID $USERNAME && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME --shell /bin/bash
RUN chown -R $USERNAME:$USERNAME /opt
#RUN mkdir /home/$USERNAME && chown -R $USERNAME:$USERNAME /home/$USERNAME

# Install jq
RUN apt-get update && apt-get install -y jq && apt-get install -y zip && apt-get install -y unzip

RUN mkdir /skechway_config/
COPY ./requirements.txt /skechway_config/

# Set the default user.
USER $USERNAME

# Switch working directory
WORKDIR /onestophub

# Install required packages
ENV PATH="/home/"$USERNAME"/.local/bin:$PATH"
RUN pip3 install -qr /skechway_config/requirements.txt

# Install node
ENV NVM_DIR="/home/"$USERNAME"/.nvm"
ENV NODE_VERSION=12.22.11
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
#RUN source $NVM_DIR/nvm.sh \
#    && nvm install $NODE_VERSION \
#    && nvm alias default $NODE_VERSION \
#    && nvm use default

# add node and npm to path so the commands are available
ENV NODE_PATH=$NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH=$NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# confirm installation
RUN node -v
RUN npm -v
RUN npm install -g speccy

# Setup poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
ENV PATH="/home/"$USERNAME"/.poetry/bin:$PATH"
RUN poetry config virtualenvs.create false