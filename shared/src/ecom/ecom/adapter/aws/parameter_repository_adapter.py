import logging
from typing import Optional
from cachetools.func import ttl_cache

from ecom.domain.repository.parameter_repository import ParameterRepository

logger = logging.getLogger(__name__)

class ParamterRepositoryAdapter(ParameterRepository):
    """Connects to the AWS paramter store and retrive the configured values."""
    
    def __init__(self, aws_parameter_client) -> None:
        """Initilize the adapter

        Args:
            aws_parameter_client (_type_): AWS SSM client
        """
        self.__parameter_client = aws_parameter_client

    @ttl_cache(ttl=600)
    def get_parameter(self, name: str) -> Optional[str]:
        param_value = None
        try:
            aws_api_response = self.__parameter_client.get_parameter(Name=name, WithDecryption=True)
            if aws_api_response:
                configured_parameter = aws_api_response.get('Parameter')
                if configured_parameter:
                    param_value = configured_parameter.get('Value')
        except self.__parameter_client.exceptions.ParameterNotFound:
            logger.warning('Missing parameter: %s', name)
        return param_value