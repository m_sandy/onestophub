from constructs import Construct
from aws_cdk import (
    aws_lambda as _lambda,
    aws_logs as _logs,
    aws_ssm as ssm,
)

LOG_LEVEL = {
    "dev": "DEBUG",
    "test": "DEBUG",
    "prod": "INFO",
}


class SkxLambda(Construct):

    @property
    def handler(self) -> _lambda.Function:
        return self._handler

    def __init__(self, scope: Construct, id: str, code: str, handler: str = "main.handler",
                 ssm_name_for_arn: str = None, memory_size: int = 384, enable_dlq: bool = False, **kwargs):
        """

        :param scope: Stack Scope for the contruct.
        :param id: Cdk ID for the resource
        :param code: Path to the lambda source code.
        :param handler: Lambda Handler function. Default lambda handler is main.handler
        :param ssm_name_for_arn: name of the SSM parameter,
                /ecommerce/{Environment}/{service_name}/{ssm_name_for_arn}/arn
        :param memory_size: Memory size for lambda in MB. Default size is 384MB
        :param enable_dlq: If True , DLQ will be configured for the lambda.
        :param kwargs:
        """
        super().__init__(scope, id, **kwargs)

        parameters = self.node.try_get_context("Parameters")
        lambda_dlq = None
        if enable_dlq:
            lambda_dlq = scope.skx_dlq

        self._handler = _lambda.Function(
            self, id,
            runtime=_lambda.Runtime.PYTHON_3_9,
            handler=handler,
            code=_lambda.Code.from_asset(code),
            memory_size=memory_size,
            log_retention=_logs.RetentionDays.ONE_DAY,
            dead_letter_queue=lambda_dlq,
            dead_letter_queue_enabled=enable_dlq
        )

        environment = parameters.get("Environment")
        is_provisioned_concurrency_active = parameters.get("LambdaProvisioningActive")
        final_provisioned_concurrency = None

        self._handler.add_environment("ENVIRONMENT", scope.skx_environment)
        self._handler.add_environment("POWERTOOLS_METRICS_NAMESPACE", f"ecommerce.{scope.skx_service_name}")
        self._handler.add_environment("POWERTOOLS_SERVICE_NAME", scope.skx_service_name)
        self._handler.add_environment("POWERTOOLS_TRACE_DISABLED", "false")
        self._handler.add_environment("LOG_LEVEL", LOG_LEVEL.get(environment))

        if is_provisioned_concurrency_active == "true":
            final_provisioned_concurrency = scope.get_env_mapping(f"{id}ProvisionedConcurrency", 1)

        # If the stack is not platform set the common lib and shared ecom layers to all lambda functions.
        if scope.skx_service_name != "platform":
            self._handler.add_environment("EVENT_BUS_NAME", scope.event_bus_name)
            self._handler.add_layers(
                _lambda.LayerVersion.from_layer_version_arn(self, "LibLayer", layer_version_arn=scope.lib_layer_arn))
            self._handler.add_layers(
                _lambda.LayerVersion.from_layer_version_arn(self, "EcomLayer",
                                                            layer_version_arn=scope.shared_ecom_layer_arn))
        if scope.service_layer_arn:
            self._handler.add_layers(
                _lambda.LayerVersion.from_layer_version_arn(self, "ServiceLayer",
                                                            layer_version_arn=scope.service_layer_arn))
        self._handler.current_version.add_alias(environment,
                                                provisioned_concurrent_executions=final_provisioned_concurrency)

        if ssm_name_for_arn:
            ssm.StringParameter(self, f"{ssm_name_for_arn}LambdaArn",
                                parameter_name=f"/ecommerce/{scope.skx_environment}/{scope.skx_service_name}"
                                               f"/{ssm_name_for_arn}/arn",
                                description=f"{ssm_name_for_arn} Lambda Arn",
                                string_value=self._handler.function_arn)
