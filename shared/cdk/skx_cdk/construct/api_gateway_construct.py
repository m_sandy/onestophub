from constructs import Construct
from aws_cdk import (
    aws_apigateway as apigateway,
    aws_apigatewayv2 as apigatewayv2,
    aws_ssm as ssm
)


class SkxApiGateway(Construct):

    @property
    def api(self) -> apigateway.RestApi:
        return self._api

    @property
    def authorizer(self) -> apigateway.CfnAuthorizer:
        return self._authorizer

    def __init__(self, scope: Construct, id: str, base_path: str = None, role: str = None,
                 **kwargs):
        super().__init__(scope, id, **kwargs)
        stack_name = scope.stack_name
        environment = scope.environment
        service_name = scope.skx_service_name

        # Stack API Gateway
        api_name = f"{stack_name}-api"
        self._api = apigateway.RestApi(self, id, rest_api_name=api_name)

        if base_path:
            apigatewayv2.CfnApiMapping(self, f"{api_name}-bp",
                                       domain_name=scope.app_parameters.get("DomainName"),
                                       api_mapping_key=base_path,
                                       stage=self._api.deployment_stage.stage_name,
                                       api_id=self._api.rest_api_id
                                       )

        auth_uri = "arn:aws:apigateway:{}:lambda:path/2015-03-31/functions/{}/invocations". \
            format(scope.region, scope.skx_authorizer)
        self._authorizer = apigateway.CfnAuthorizer(self, "SkxAuthorizer",
                                                    authorizer_credentials=role,
                                                    name=f"{api_name}-auth",
                                                    rest_api_id=self._api.rest_api_id,
                                                    type="REQUEST",
                                                    auth_type="apiKey",
                                                    authorizer_uri=auth_uri,
                                                    identity_source="method.request.header.Authorization, "
                                                                    "method.request.header.StoreId",
                                                    authorizer_result_ttl_in_seconds=100
                                                    )

        # Store API Url in parameter store
        api_url = f"https://{self._api.rest_api_id}.execute-api.{scope.skx_region}.amazonaws.com/prod"
        ssm.StringParameter(self, "ApiUrl",
                            parameter_name=f"/ecommerce/{scope.skx_environment}/{service_name}/api/url",
                            description=f"{stack_name} API Url",
                            string_value=api_url)

        # Store API Arn in parameter store
        api_arn = scope.format_arn(
            service="execute-api",
            resource=self._api.rest_api_id,
            resource_name="prod"
        )
        ssm.StringParameter(self, "ApiArn",
                            parameter_name=f"/ecommerce/{scope.skx_environment}/{service_name}/api/arn",
                            description=f"{stack_name} API Arn",
                            string_value=api_arn)
