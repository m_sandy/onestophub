from constructs import Construct
from aws_cdk import (
    aws_apigateway as apigateway,
)
from cdk_watchful import Watchful


class DashboardGateway(Construct):

    @property
    def authorizer(self) -> apigateway.CfnAuthorizer:
        return self._authorizer

    def __init__(self, scope: Construct, id: str, base_path: str = None, role: str = None,
                 **kwargs):
        super().__init__(scope, id, **kwargs)
        stack_name = scope.stack_name
        environment = scope.environment
        service_name = scope.skx_service_name

        # Dashboard
        self.__watchful = Watchful(self, "monitoring", alarm_sns=scope.skx_alarm_topic, dashboard_name=stack_name)
