from typing import List, Union, Optional
from abc import abstractmethod
import os
from aws_cdk import (
    Stack, Duration,
    aws_iam as iam,
    aws_sns as sns,
    aws_sqs as sqs,
    aws_cloudwatch as cloudwatch,
    aws_cloudwatch_actions as cloudwatch_actions,
    CfnParameter, RemovalPolicy
)

from aws_cdk import aws_lambda as lambda_
from constructs import Construct

from skx_cdk.skx_base_stack import SkxBaseStack


class SkxApiStack(SkxBaseStack):

    @property
    def skx_event_bus_arn(self) -> str:
        return self.__event_bus_arn

    @property
    def event_bus_name(self) -> str:
        return self._event_bus_name

    @property
    def event_bridge_principal(self) -> iam.ServicePrincipal:
        return self.__event_bridge_principal

    @property
    def event_bridge_role(self) -> iam.Role:
        return self.__event_bridge_role

    @property
    def apigateway_principal(self) -> iam.ServicePrincipal:
        return self.__apigateway_principal

    @property
    def lib_layer_arn(self) -> str:
        return self._lib_layer_arn.value_as_string

    @property
    def shared_ecom_layer_arn(self) -> str:
        return self._shared_ecom_layer_arn.value_as_string

    @property
    def skx_authorizer(self) -> str:
        return self.__skx_authorizer

    @property
    def skx_account(self):
        return self.__skx_account

    @property
    def skx_region(self):
        return self.__skx_region

    @property
    def skx_alarm_topic(self) -> sns.Topic:
        return self.__alarm_topic

    @property
    def skx_dlq(self) -> sqs.Queue:
        return self.__skx_dlq

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        self.__final_env_mapping = None

        self.__event_bridge_principal = iam.ServicePrincipal("events.amazonaws.com")
        self.__apigateway_principal = iam.ServicePrincipal("apigateway.amazonaws.com")

        self.__event_bridge_role = iam.Role(self, "EventBridgeRole",
                                            assumed_by=iam.ServicePrincipal("events.amazonaws.com"))

        # The code that defines your stack goes here
        stack_name = self.stack_name

        self.__skx_account = Stack.of(self).account
        self.__skx_region = Stack.of(self).region

        # -------- Stack parameters ------------
        CfnParameter(self, "VpcSubnet", type="AWS::SSM::Parameter::Value<List<AWS::EC2::Subnet::Id>>",
                     description="VPC Subnet.")

        CfnParameter(self, "VpcSg", type="AWS::SSM::Parameter::Value<AWS::EC2::SecurityGroup::Id>",
                     description="VPC Security Group.")

        event_bus_arn = CfnParameter(self, "EventBusArn", type="AWS::SSM::Parameter::Value<String>",
                                     description="Event Bus Arn")
        self.__event_bus_arn = event_bus_arn.value_as_string

        event_bus_name = CfnParameter(self, "EventBusName", type="AWS::SSM::Parameter::Value<String>",
                                      description="Event Bus Name")
        self._event_bus_name = event_bus_name.value_as_string

        skx_authorizer = CfnParameter(self, "AuthorizerArn", type="AWS::SSM::Parameter::Value<String>",
                                      description="Authorizer Arn")
        self.__skx_authorizer = skx_authorizer.value_as_string

        self._lib_layer_arn = CfnParameter(self, "CommonLibLayerArn", type="AWS::SSM::Parameter::Value<String>",
                                           description="Libraries Arn")

        self._shared_ecom_layer_arn = CfnParameter(self, "SharedEcomLayerArn",
                                                   type="AWS::SSM::Parameter::Value<String>",
                                                   description="Common shared platform code")

        has_service_layer = os.path.isdir("service_layer/")
        if has_service_layer:
            service_layer = lambda_.LayerVersion(self, "ServiceLayer",
                                                 removal_policy=RemovalPolicy.RETAIN,
                                                 code=lambda_.Code.from_asset("service_layer/"),
                                                 layer_version_name=f"{stack_name.lower()}-service-layer"
                                                 )
            self._service_layer_arn = service_layer.layer_version_arn

        # ----------------- Alarm Topic ---------------
        self.__alarm_topic = sns.Topic(
            self,
            "AlarmTopic",
            display_name=f"{self.skx_service_name}"
        )

        self.__skx_dlq = sqs.Queue(
            self, "SkxDlq"
        )

        alarm = cloudwatch.Alarm(self, "DlqAlarm",
                                 actions_enabled=True,
                                 comparison_operator=cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
                                 threshold=1,
                                 evaluation_periods=1,
                                 treat_missing_data=cloudwatch.TreatMissingData.NOT_BREACHING,
                                 metric=cloudwatch.Metric(
                                     dimensions_map={"Name": "QueueName", "Value": self.__skx_dlq.queue_name},
                                     metric_name="NumberOfMessagesSent",
                                     namespace="AWS/SQS",
                                     period=Duration.seconds(300),
                                     statistic="Sum",
                                     unit=cloudwatch.Unit.COUNT
                                 )
                                 )
        alarm.add_alarm_action(cloudwatch_actions.SnsAction(self.__alarm_topic))

    def get_api_lambda_invoke_role(self, resources: List[str]) -> iam.Role:
        # API Roles
        api_lambda_invoke_policy_stmt = iam.PolicyStatement(
            effect=iam.Effect.ALLOW,
            actions=["lambda:InvokeFunction"],
            resources=resources
        )

        api_lambda_invoke_policy = iam.ManagedPolicy(
            self,
            "AuthLambdaInvoke",
            statements=[api_lambda_invoke_policy_stmt]
        )

        rest_api_role: iam.Role = iam.Role(
            self,
            "RestAPIRole",
            assumed_by=iam.ServicePrincipal("apigateway.amazonaws.com"),
            managed_policies=[api_lambda_invoke_policy]
        )
        return rest_api_role

    @staticmethod
    def get_base_env_mapping():
        env_mapping = {
            "dev": {
                "LogLevelMapping": "DEBUG",
                "QueueMessageRetention": 3600,
                "QueueMaxReceiveCount": 1,
                "QueueMessageVisibilityTimeout": 900,
            },
            "test": {
                "LogLevelMapping": "DEBUG",
                "QueueMessageRetention": 172800,
                "QueueMaxReceiveCount": 3,
                "QueueMessageVisibilityTimeout": 900,
            },
            "prod": {
                "LogLevelMapping": "INFO",
                "QueueMessageRetention": 172800,
                "QueueMaxReceiveCount": 3,
                "QueueMessageVisibilityTimeout": 900,
            },
        }
        return env_mapping

    @abstractmethod
    def env_mapping(self) -> dict:
        pass

    def get_env_mapping(self, key: str, default_value: Optional[Union[str, int]] = None) -> Union[str, int, None]:
        if self.__final_env_mapping is None:
            self.__final_env_mapping = self.dict_merge(self.get_base_env_mapping(), self.env_mapping())
        local_env_mapping = self.__final_env_mapping.get(self.skx_environment, dict())
        return local_env_mapping.get(key, default_value)

    @staticmethod
    def dict_merge(main_dict: dict, merge_dict: dict) -> dict:
        """ Recursive dict merge. Dict_merge recurses down into dicts nested
        to an arbitrary depth, updating keys. The ``main_dict`` is merged into
        ``main_dict``.

        :param main_dict: Dict onto which the merge is executed
        :param merge_dict: mege_dict merged into main_dict
        :return: Merged main_dict
        """
        for key in merge_dict.keys():
            if key in main_dict and isinstance(main_dict[key], dict) and isinstance(merge_dict[key], dict):
                SkxApiStack.dict_merge(main_dict[key], merge_dict[key])
            else:
                main_dict[key] = merge_dict[key]
        return main_dict
