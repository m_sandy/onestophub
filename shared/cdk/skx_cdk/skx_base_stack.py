"""
Base class for setting the Skechers AWS stack.
"""
from constructs import Construct

from aws_cdk import (
    Stack,
    CfnParameter,
)


class SkxBaseStack(Stack):

    @property
    def app_parameters(self) -> dict:
        return self.__parameters

    @property
    def skx_environment(self) -> str:
        return self.__parameters.get("Environment")

    @property
    def service_layer_arn(self) -> str:
        return self._service_layer_arn

    @property
    def skx_service_name(self) -> str:
        local_service_name = self.stack_name
        if self.__tags and self.__tags.get("Service"):
            local_service_name = self.__tags.get("Service")
        return local_service_name

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        self.__tags = self.node.try_get_context("Tags")
        self.__parameters = self.node.try_get_context("Parameters")
        self._service_layer_arn = None

        CfnParameter(self, "Environment", type="String", description="Service environment",
                     allowed_values=[
                         'dev', 'test', 'prod'
                     ])

        CfnParameter(self, "LogLevel", type="String", description="Lambda Log level")

        CfnParameter(self, "RetentionInDays", type="Number", description="Logs retention in days")

        CfnParameter(self, "LambdaProvisioningActive", type="String", description="API custom domain name")

        CfnParameter(self, "DomainName", type="String", description="API custom domain name")
