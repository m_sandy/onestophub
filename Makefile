# Setup variables
NAME = one-stop-hub
PYENV := $(shell which pyenv)
SPECCY := $(shell which speccy)
JQ := $(shell which jq)
PYTHON_VERSION = 3.9.10
MAKEOPTS += -j4
DOCKER := $(shell which docker-compose)

# Note. If you change this, you also need to update docker-compose.yml.
DOCKER_SERVICE_NAME := onestophub

# Service variables
SETUP_INIT := $(shell tools/setup-init)
SERVICES = $(shell tools/services)
SERVICES_ENVONLY = $(shell tools/services --env-only)
export DOMAIN ?= ecommerce
export ENVIRONMENT ?= dev

# Colors
ccblue = \033[0;96m
ccend = \033[0m

###################
# SERVICE TARGETS #
###################

# Run pipeline on services
all:
	@for service_line in $(shell tools/services --graph --env-only); do \
		${MAKE} ${MAKEOPTS} $$(echo all-$$service_line | sed 's/,/ all-/g') QUIET=true || exit 1 ; \
	done
all-%:
	@${MAKE} lint-$*
	@${MAKE} build-$*
	@${MAKE} tests-unit-$*
	@${MAKE} check-deps-$*
	@${MAKE} package-$*
	@${MAKE} deploy-$*
	@${MAKE} tests-integ-$*

pdeploy-%:
	@${MAKE} build-$*
	@${MAKE} tests-unit-$*
	@${MAKE} check-deps-$*
# 	@${MAKE} package-$*
	@${MAKE} cdk-deploy-$*
	@${MAKE} tests-integ-$*

bpdeploy-%:
	@${MAKE} build-$*
	@${MAKE} check-deps-$*
	@${MAKE} package-$*
	@${MAKE} cdk-deploy-$*

bdeploy-%:
	@${MAKE} build-$*
	@${MAKE} check-deps-$*
	@${MAKE} cdk-deploy-$*

# Run CI on services
ci: $(foreach service,${SERVICES}, ci-${service})
ci-%:
	@${MAKE} lint-$*
	@${MAKE} build-$*
	@${MAKE} tests-unit-$*

# All but for dependencies
deps-%:
	@for service_line in $(shell tools/services --graph --env-only --deps-of $*); do \
		${MAKE} ${MAKEOPTS} $$(echo all-$$service_line | sed 's/,/ all-/g') QUIET=true || exit 1 ; \
	done

# Artifacts services
artifacts: $(foreach service,${SERVICES_ENVONLY}, all-${service})
artifacts-%:
	@echo "[*] $(ccblue)artifacts $*$(ccend)"
	@${MAKE} -C $* artifacts

# Build services
build: $(foreach service,${SERVICES}, build-${service})
build-%:
	@echo "[*] $(ccblue)build $*$(ccend)"
	@${MAKE} -C $* build

# Check-deps services
check-deps: $(foreach service,${SERVICES_ENVONLY}, check-deps-${service})
check-deps-%:
	@echo "[*] $(ccblue)check-deps $*$(ccend)"
	@${MAKE} -C $* check-deps

# Clean services
clean: $(foreach service,${SERVICES}, clean-${service})
clean-%:
	@echo "[*] $(ccblue)clean $*$(ccend)"
	@${MAKE} -C $* clean

deploy: $(foreach service,${SERVICES_ENVONLY}, deploy-${service})
deploy-%:
	@echo "[*] $(ccblue)deploy $*$(ccend)"
	@${MAKE} -C $* deploy

# Lint services
lint: $(foreach service,${SERVICES}, lint-${service})
lint-%:
	@echo "[*] $(ccblue)lint $*$(ccend)"
	@${MAKE} -C $* lint

# Package services
package: $(foreach service,${SERVICES_ENVONLY}, package-${service})
package-%:
	@echo "[*] $(ccblue)package $*$(ccend)"
	@${MAKE} -C $* package

# Teardown services
teardown:
	@for service_line in $(shell tools/services --graph --reverse --env-only); do \
		${MAKE} ${MAKEOPTS} $$(echo teardown-$$service_line | sed 's/,/ teardown-/g') QUIET=true || exit 1 ; \
	done
teardown-%:
	@echo "[*] $(ccblue)teardown $*$(ccend)"
	@${MAKE} -C $* teardown

# Integration tests
tests-integ: $(foreach service,${SERVICES_ENVONLY}, tests-integ-${service})
tests-integ-%:
	@echo "[*] $(ccblue)tests-integ $*$(ccend)"
	@${MAKE} -C $* tests-integ

# Unit tests
tests-unit: $(foreach service,${SERVICES}, tests-unit-${service})
tests-unit-%:
	@echo "[*] $(ccblue)tests-unit $*$(ccend)"
	@${MAKE} -C $* tests-unit

# End-to-end tests
tests-e2e:
	@tools/tests-e2e

# Performance tests
tests-perf:
	@tools/tests-perf

#################
# SETUP TARGETS #
#################

# Validate that necessary tools are installed
validate: validate-pyenv validate-jq

# Validate that pyenv is installed
validate-pyenv:
ifndef PYENV
	$(error Make sure pyenv is accessible in your path. You can install pyenv by following the instructions at 'https://github.com/pyenv/pyenv-installer'.)
endif
ifndef PYENV_SHELL
	$(error Add 'pyenv init' to your shell to enable shims and autocompletion.)
endif
ifndef PYENV_VIRTUALENV_INIT
	$(error Add 'pyenv virtualenv-init' to your shell to enable shims and autocompletion.)
endif

# Validate that speccy is installed
# Speccy is a linter built by WeWork to validate and make recommendations, like eslint, but for OpenAPI
validate-speccy:
ifndef SPECCY
	$(error 'speccy' not found. You can install speccy by following the instructions at 'https://github.com/wework/speccy'.)
endif

# Validate that jq is installed
# jq is a lightweight and flexible command-line JSON processor.
validate-jq:
ifndef JQ
	$(error 'jq' not found. You can install jq by following the instructions at 'https://stedolan.github.io/jq/download/'.)
endif

# setup: configure tools
setup: validate
	@echo "[*] Download and install python $(PYTHON_VERSION)"
	@pyenv install $(PYTHON_VERSION)
	@pyenv local $(PYTHON_VERSION)
	@echo "[*] Create virtualenv $(NAME) using python $(PYTHON_VERSION)"
	@pyenv virtualenv $(PYTHON_VERSION) $(NAME)
	@$(MAKE) activate
	@$(MAKE) requirements
	@${MAKE} npm-install
	@echo "[*] Install poetry"
	@curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -

# Activate the virtual environment
activate: validate-pyenv
	@echo "[*] Activate virtualenv $(NAME)"
	$(shell eval "$$(pyenv init -)" && eval "$$(pyenv virtualenv-init -)" && pyenv activate $(NAME) && pyenv local $(NAME))

# Install python dependencies
requirements:
	@echo "[*] Install Python requirements"
	@pip3 install -r requirements.txt

# Install npm dependencies
npm-install:
	@echo "[*] Install NPM tools"
	@npm install -g speccy

# Pip setup
pip-setup: $(foreach service,${SERVICES}, pip-setup-${service})
pip-setup-%:
	@echo "[*] $(ccblue)pip-setup $*$(ccend)"
	@${MAKE} -C $* pip-setup

# Poetry setup
poetry-update: $(foreach service,${SERVICES}, poetry-update-${service})
poetry-update-%:
	@echo "[*] $(ccblue)poetry-update $*$(ccend)"
	@${MAKE} -C $* poetry-update

# Poetry setup
poetry-install: $(foreach service,${SERVICES}, poetry-install-${service})
poetry-install-%:
	@echo "[*] $(ccblue)poetry-install $*$(ccend)"
	@${MAKE} -C $* poetry-install

install-dev-packages: 
	$(foreach service,${SERVICES}, tools/local-package-install ${service})

# Docker Commands
validate-docker-env:
ifndef DOCKER
	$(error $(RED)Make sure docker-compose is accessible in your path. You can install docker-compose by following the instructions at $(GREEN)'https://docs.docker.com/compose/install'. $(RESET))
endif

docker-rebuild: ##1 Clean and rebuild the docker container (force a rebuild by passing --no-cache).
	@docker-compose build --no-cache

docker-build: validate-docker-env
	@docker-compose build

docker-login: validate-docker-env ##1 Login to the Docker container. NOTE: Use this to run tests-unit inside Docker container to make the tests run fast.
	@docker exec -it -u onestophubuser $(DOCKER_SERVICE_NAME) "/bin/bash"

docker-logs: validate-docker-env
	@echo "[*] $(ccblue)Tailing docker logs $*$(ccend)"
	@docker-compose -f docker-compose.yaml logs --tail=100 -f $(c)

docker-stop: validate-docker-env ##1 Stop docker containers.
	@docker-compose -f docker-compose.yaml stop $(c)

docker-down: validate-docker-env
	@docker-compose -f docker-compose.yaml down $(c)

docker-destroy: validate-docker-env
	@docker-compose -f docker-compose.yaml down -v $(c)

docker-up: validate-docker-env ##1 Create and start all the containers required for the development and unit tests in detach mode.
	@docker-compose -f docker-compose.yaml up -d

docker-start: validate-docker-env
	@echo "$(ccblue)Start Docker$(ccend)"
	@docker-compose -f docker-compose.yaml start $(c)

docker-restart: docker-stop docker-up ##1 Restart all the containers required for the development and unit tests in detach mode.

docker-prune: validate-docker-env ##1 Clean all docker containers that is not actively used.
	docker system prune -af

create-service:
	@tools/create-service -sn=$(name)

create-resource:
	@tools/create-resource -sn=$(service)

# cdk setup
cdk-synth: $(foreach service,${SERVICES}, cdk-synth-${service})
cdk-synth-%:
	@echo "[*] $(ccblue)cdk-synth $*$(ccend)"
	@${MAKE} -C $* cdk-synth

cdk-bootstrap: $(foreach service,${SERVICES}, cdk-bootstrap-${service})
cdk-bootstrap-%:
	@echo "[*] $(ccblue)cdk-bootstrap $*$(ccend)"
	@${MAKE} -C $* cdk-bootstrap

cdk-deploy: $(foreach service,${SERVICES}, cdk-deploy-${service})
cdk-deploy-%:
	@echo "[*] $(ccblue)cdk-deploy $*$(ccend)"
	@${MAKE} -C $* cdk-deploy
