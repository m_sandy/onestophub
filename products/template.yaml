AWSTemplateFormatVersion: "2010-09-09"
Transform: 'AWS::Serverless-2016-10-31'


Parameters:
  Environment:
    Type: String
    Default: dev
    Description: Environment name
  LogLevel:
    Type: String
    Default: INFO
  RetentionInDays:
    Type: Number
    Default: 30
    Description: CloudWatch Logs retention period for Lambda functions
  EventBusArn:
    Type: AWS::SSM::Parameter::Value<String>
    Description: EventBridge Event Bus ARN
  AuthorizerArn:
    Type: AWS::SSM::Parameter::Value<String>
    Description: Authorizer ARN
  EventBusName:
    Type: AWS::SSM::Parameter::Value<String>
    Description: EventBridge Event Bus Name
  CommonLibLayerArn:
    Type: AWS::SSM::Parameter::Value<String>
    Description: Core Dependencies Layer
  SharedEcomLayerArn:
    Type: AWS::SSM::Parameter::Value<String>
    Description: Shared ecom layer
  DomainName:
    Type: String
    Description: Domain to which api path will be mapped.
  LambdaProvisioningActive:
    Type: String
    Default: false
    Description: Set true/false to enable/disable the lambda provisioned concurrency.

Mappings:
  ConcurrencyMap:
    dev:
      ValidateFunction: 1
    test:
      ValidateFunction: 1
    prod:
      ValidateFunction: 1

Conditions:
  IsNotProd: !Not [!Equals [!Ref Environment, prod]]
  IsLambdaProvisioningActive: !Equals [!Ref LambdaProvisioningActive, true]

Globals:
  Function:
    Runtime: python3.9
#    Architectures:
#      - arm64
    Handler: main.handler
    Timeout: 30
    Tracing: Active
    Environment:
      Variables:
        ENVIRONMENT: !Ref Environment
        EVENT_BUS_NAME: !Ref EventBusName
        POWERTOOLS_SERVICE_NAME: products
        POWERTOOLS_TRACE_DISABLED: "false"
        LOG_LEVEL: !Ref LogLevel
    Layers:
      - !Ref CommonLibLayerArn
      - !Ref SharedEcomLayerArn


Resources:
  #############
  # FUNCTIONS #
  #############
  ValidateFunction:
    Type: AWS::Serverless::Function
    Properties:
      AutoPublishAlias: !Ref Environment
      ProvisionedConcurrencyConfig: !If [IsLambdaProvisioningActive, ProvisionedConcurrentExecutions: !FindInMap [ConcurrencyMap, !Ref Environment, ValidateFunction], !Ref AWS::NoValue]
      CodeUri: src/validate/
      Events:
        BackendApi:
          Type: Api
          Properties:
            Path: /backend/validate
            Method: POST
            RestApiId: !Ref Api
      Policies:
        - arn:aws:iam::aws:policy/CloudWatchLambdaInsightsExecutionRolePolicy

  ValidateLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Sub "/aws/lambda/${ValidateFunction}"
      RetentionInDays: !Ref RetentionInDays


  ###############
  # API GATEWAY #
  ###############
  Api:
    Type: AWS::Serverless::Api
    Properties:
      DefinitionBody:
        Fn::Transform:
          Name: "AWS::Include"
          Parameters:
            Location: "resources/openapi.yaml"
      EndpointConfiguration: REGIONAL
      StageName: prod
      TracingEnabled: true

  ProductsApiMapping:
    Type: 'AWS::ApiGatewayV2::ApiMapping'
    Properties:
      DomainName: !Ref DomainName
      ApiMappingKey: 'v1/products'
      ApiId: !Ref Api
      Stage: !Ref Api.Stage

  ApiArnParameter:
    Type: AWS::SSM::Parameter
    Properties:
      Name: !Sub /ecommerce/${Environment}/products/api/arn
      Type: String
      Value: !Sub "arn:${AWS::Partition}:execute-api:${AWS::Region}:${AWS::AccountId}:${Api}/prod"

  ApiUrlParameter:
    Type: AWS::SSM::Parameter
    Properties:
      Name: !Sub /ecommerce/${Environment}/products/api/url
      Type: String
      Value: !Sub "https://${Api}.execute-api.${AWS::Region}.amazonaws.com/prod"

  #####################
  # DEAD LETTER QUEUE #
  #####################
#  DeadLetterQueue:
#    Type: AWS::CloudFormation::Stack
#    Properties:
#      # The path starts with '../..' as this will be evaluated from the
#      # products/build folder, not the products folder.
#      TemplateURL: ../../shared/templates/dlq.yaml
