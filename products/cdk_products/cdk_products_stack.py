from aws_cdk import (
    aws_ssm as ssm,
    aws_apigateway as apigateway,
    aws_cloudwatch as cloudwatch,
)
from cdk_watchful import Watchful
from constructs import Construct
from skx_cdk.skx_api_stack import SkxApiStack
from skx_cdk.construct.lambda_construct import SkxLambda
from skx_cdk.construct.api_gateway_construct import SkxApiGateway


class CdkProductsStack(SkxApiStack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        stack_name = self.stack_name

        # -------------- Lambda Setup -------------------
        validate_lambda_construct = SkxLambda(self, "ValidateFunction", code="src/validate/")
        api_lambda_invoke_role = self.get_api_lambda_invoke_role([
            self.skx_authorizer,
            validate_lambda_construct.handler.function_arn
        ])

        # ------- API Setup ------------------
        api_name = f"{stack_name}-api"
        skx_apigateway_construct = SkxApiGateway(self, api_name, base_path="v1/products",
                                                 role=api_lambda_invoke_role.role_arn)
        api = skx_apigateway_construct.api

        # ------- Lambda API Mapping ------------------
        backend_resource = api.root.add_resource('backend')
        validate_resource = backend_resource.add_resource('validate')
        validate_resource.add_method("POST", apigateway.LambdaIntegration(validate_lambda_construct.handler),
                                     authorization_type=apigateway.AuthorizationType.IAM,
                                     )

        # --------------- Monitoring -----------------
        watchful = Watchful(self, "monitoring", alarm_sns=self.skx_alarm_topic, dashboard_name=stack_name)
        watchful.add_widgets(cloudwatch.TextWidget(markdown="# Key Metrics", width=24, height=1),
                             cloudwatch.SingleValueWidget(
                                 metrics=[cloudwatch.Metric(
                                     metric_name="productsCreated",
                                     label="Products Created",
                                     namespace=f"ecommerce.{self.skx_service_name}",
                                     dimensions_map={
                                         "environment": self.skx_environment,
                                         "service": self.skx_service_name,
                                     }
                                 )]
                             ))
        watchful.watch_api_gateway(title="Product API", rest_api=api)
        watchful.add_widgets(cloudwatch.TextWidget(markdown="# Lambda Metrics", width=24, height=1))
        watchful.watch_lambda_function("Validate Products Function", validate_lambda_construct.handler)

    def env_mapping(self) -> dict:
        env_mapping = {
            "dev": {
                "ValidateFunctionProvisionedConcurrency": 1,
            },
            "test": {
                "ValidateFunctionProvisionedConcurrency": 1,
            },
            "prod": {
                "ValidateFunctionProvisionedConcurrency": 1,
            },
        }
        return env_mapping
