"""
ValidateFunction
"""


import json
import os
from typing import List, Optional, Union, Set
from boto3.dynamodb.types import TypeDeserializer
from aws_lambda_powertools.tracing import Tracer
from aws_lambda_powertools.logging.logger import Logger
from ecom.apigateway import iam_user_id, response # pylint: disable=import-error


ENVIRONMENT = os.environ["ENVIRONMENT"]



logger = Logger() # pylint: disable=invalid-name
tracer = Tracer() # pylint: disable=invalid-name
type_deserializer = TypeDeserializer() # pylint: disable=invalid-name


ddb_products = ['P001', 'P002', 'P003', 'P004']

@tracer.capture_method
def validate_products(products: List[dict]) -> Set[Union[List[dict], str]]:
    """
    Takes a list of products and validate them

    If all products are valid, this will return an empty list.
    """

    validated_products = []
    reasons = []

    # batch_get_item only supports up to 100 items, so split the list of products in batches
    # of 100 max.
    for i in range(0, len(products), 100):
        q_products = {}
        for product in products[i:i+100]:
            q_products[product["productId"]] = product


        for product_id, product in q_products.items():
            if product_id not in ddb_products:
                validated_products.append(product_id)
                reasons.append("Product '{}' not found".format(product_id))

    return validated_products, ". ".join(reasons)


@logger.inject_lambda_context
@tracer.capture_lambda_handler
def handler(event, _):
    """
    Lambda function handler for /backend/validate
    """

    # user_id = iam_user_id(event)
    # if user_id is None:
    #     logger.warning({"message": "User ARN not found in event"})
    #     return response("Unauthorized", 401)

    # Extract the list of products
    try:
        body = json.loads(event["body"])
    except Exception as exc: # pylint: disable=broad-except
        logger.warning("Exception caught: %s", exc)
        return response("Failed to parse JSON body", 400)

    if "products" not in body:
        return response("Missing 'products' in body", 400)

    products, reason = validate_products(body["products"])

    if len(products) > 0:
        return response({
            "message": reason,
            "products": products
        }, 200)

    return response("All products are valid")
