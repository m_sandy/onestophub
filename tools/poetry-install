#!/bin/bash

set -e

ROOT=${ROOT:-$(pwd)}
SERVICE=$1

service_dir=$ROOT/$SERVICE

display_usage () {
    echo "Usage: $0 TYPE SERVICE"
}

# Check if there are at least 2 arguments
if [ $# -lt 1 ]; then
    display_usage
    exit 1
fi

# Check if the service exists
if [ ! -f $service_dir/metadata.yaml ]; then
    echo "Service $SERVICE does not exist"
    exit 1
fi

poetry_install () {
    
    # install source files
    if [ -d $service_dir/src/ ]; then
        for function in $(find $service_dir/src/ -name "pyproject.toml"); do
            if [ -f $function ]; then
                full_dir_path="$(dirname "$function")"
                cd $full_dir_path
                poetry install
                cd $ROOT
            else
                echo "No poetry pyproject.toml file for $function."
            fi
        done
    fi

    # install service_layer files
    for folder_name in "service_layer" "lib_layer" "shared_layer"
    do
        if [ -d $service_dir/$folder_name/ ]; then
            if [ -f $service_dir/$folder_name/python/pyproject.toml ]; then
                cd $service_dir/$folder_name/python
                poetry install
                cd $ROOT
            fi
        else
            echo "No $folder_name. Skipping"
        fi
    done

}

poetry_install