import toml
import sys, getopt


def main(argv):
    inputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('alter_service_toml.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('alter_service_toml.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
    print('Input file is "', inputfile)

    data = toml.load(inputfile) 
    # # Modify field
    # data['tool']['poetry']['dependencies']['ecom']['path'] = '../../../../shared/src/ecom'
    # updated_file = open(inputfile, 'w')
    # toml.dump(data, updated_file)
    # updated_file.close()
    poetry_lock_file = open(inputfile, "r")
    list_of_lines = poetry_lock_file.readlines()
    
    updated_file = open(inputfile, "w")
    for line in list_of_lines:
        if line == 'url = "../../../shared/src/ecom"':
            print("Found the line ..........")
            print("*************************")
            print("*************************")
            line = 'url = "../../../../shared/src/ecom"'
        
        updated_file.write(line)
    updated_file.close()


if __name__ == "__main__":
    main(sys.argv[1:])